package cn.com.mypm.bugManager.service;

import cn.com.mypm.framework.app.services.BaseService;
import cn.com.mypm.object.BugBaseInfo;
import cn.com.mypm.testTaskManager.dto.CurrTaskInfo;

public interface BugFlowControlService extends BaseService {

	public void upInitContl(CurrTaskInfo currTaskInfo,BugBaseInfo bug);
}
