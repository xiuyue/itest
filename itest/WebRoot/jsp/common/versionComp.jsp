<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
	<div id="versionCompareWin" style="display:none;">
		<div id="versionCompareGridDiv" style="width:985px;"></div>
		<div>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td width="40%" height="30px" align="left"><div id="versionMessage" class="message" style="text-align:left;padding-left:5px;">&nbsp;</div></td>
					<td><div id="versionCompareText" class="waring">&nbsp;</div></td>
					<td width="40%" align="right" style="padding-right:5px;">
						<img id="versionCompareImg" src="<%=request.getContextPath()%>/jsp/common/images/compare.gif" onclick="versionCompareMember();" title="版本比较" style="cursor:pointer;" />&nbsp;
					</td>
				</tr>
			</table>
		</div>
		<div id="versionCompareMemberDiv" style="width:985px;"></div>
	</div>
<script type="text/javascript">
var versonCompareW_ch, projectGrid, vcGrid;
function versionCompareInit(flag){
		var versionLogId = pmGrid.getSelectedId();
		if(versionLogId != null){
		}else{
			showMessage(false, "项目版本比较", "请选择项目进行比较");return;
		}
		versonCompareW_ch = initW_ch(versonCompareW_ch,  "versionCompareWin", false, 995, 560, "", "项目版本比较");
		$("versionMessage").innerHTML = "";
		if(typeof(vcGrid) != "undefined"){vcGrid.clearAll();}
		if(typeof(projectGrid) == "undefined"){
			//var nameWidth_c = 120 - scrollWidth;
			projectGrid = new dhtmlXGridObject('versionCompareGridDiv');
			projectGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
		    projectGrid.setHeader("&nbsp;,&nbsp;,版本编号,项目名称,项目负责人,优先级,计划开始时间,实际开始时间,计划结束时间,实际结束时间,是否完成计划,进度(%),&nbsp;");
		    projectGrid.setInitWidths("40,40,80,120,100,60,95,95,95,95,95,70,0");
		    projectGrid.setColAlign("center,center,left,left,left,center,center,center,center,center,center,center,center");
		    projectGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
		    projectGrid.setColSorting("int,int,str,str,str,str,str,str,str,str,str,int,int");
			projectGrid.enableAutoHeight(true, 200);
		    projectGrid.init();
		    projectGrid.enableTooltips("false,false,false,false,true,true,true,true,true,true,true,true,false");
		    projectGrid.setSkin("light");
    	}
    	if(flag)versionLogId = pmGrid.cells(versionLogId,14).getValue();
		if(versionLogId == "")return;
		var result = dhtmlxAjax.postSync(conextPath + "/project/projectAction!versionCompareInit.action?projectDto.project.id=" + versionLogId, "").xmlDoc.responseText;
		if(result != "failed" && result != ""){
			parseCustomDatas(projectGrid, result);
			versonCompareW_ch.setDimension(995, $("versionCompareWin").offsetHeight + 25);
		}else{
			showMessage(false, "项目版本比较", "此项目暂无版本历史");
			//versonCompareW_ch.hide();
		}
}

function versionCompareMember(){
	var checkProjectId = getChecked(projectGrid, 0);
	if(checkProjectId.indexOf(",") == -1){
		$("versionCompareText").innerHTML = "请选择两个版本进行比较";
		return;
	}
	if(checkProjectId.indexOf(",") != checkProjectId.lastIndexOf(",")){
		$("versionCompareText").innerHTML = "请选择两个版本进行比较";
		return;
	}
	$("versionCompareText").innerHTML = "&nbsp;";
	if(typeof(vcGrid) == "undefined"){
		vcGrid = new dhtmlXGridObject('versionCompareMemberDiv');
		vcGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
		vcGrid.setHeader("&nbsp;,<div title=' - 为版本二中被删除 + 为版本二中新增'>&nbsp;</div>,任务名称,计划开始时间,#cspan,计划结束时间,#cspan,计划工时,#cspan,#cspan,计划成本,#cspan,#cspan,计划资源,#cspan,#cspan,#cspan");
		vcGrid.attachHeader("#rspan,#rspan,#rspan,版本一,版本一,版本一,版本一,版本一,版本一,差异,版本一,版本一,差异,版本一,版本一,差异,&nbsp;");
		//var nameWidth_c = 115 - scrollWidth;
		vcGrid.setInitWidths("0,40,*,75,75,75,75,50,50,50,60,60,60,80,80,40,0");
		vcGrid.setColAlign("center,center,left,center,center,center,center,center,center,center,center,center,center,left,left,center,center");
		vcGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,dyn,ro,ro,dyn,ro,ro,ro,ro");
		vcGrid.enableAutoHeight(true, 300);
		vcGrid.init();
		vcGrid.enableRowsHover(true, "red");
		vcGrid.enableTooltips("false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false");
		vcGrid.setSkin("light");
		vcGrid.hdr.rows[1].cells[3].firstChild.align = "center";
		vcGrid.hdr.rows[1].cells[4].firstChild.align = "center";
		vcGrid.hdr.rows[1].cells[5].firstChild.align = "center";
		vcGrid.hdr.rows[1].cells[6].firstChild.align = "center";
		vcGrid.hdr.rows[1].cells[7].firstChild.align = "center";
		vcGrid.hdr.rows[2].cells[0].innerHTML = "<div align=center>版本一</div>";
		vcGrid.hdr.rows[2].cells[1].innerHTML = "<div align=center>版本二</div>";
		vcGrid.hdr.rows[2].cells[2].innerHTML = "<div align=center>版本一</div>";
		vcGrid.hdr.rows[2].cells[3].innerHTML = "<div align=center>版本二</div>";
		vcGrid.hdr.rows[2].cells[4].innerHTML = "<div align=center>版本一</div>";
		vcGrid.hdr.rows[2].cells[5].innerHTML = "<div align=center>版本二</div>";
		vcGrid.hdr.rows[2].cells[6].innerHTML = "<div align=center>差异</div>";
		vcGrid.hdr.rows[2].cells[7].innerHTML = "<div align=center>版本一</div>";
		vcGrid.hdr.rows[2].cells[8].innerHTML = "<div align=center>版本二</div>";
		vcGrid.hdr.rows[2].cells[9].innerHTML = "<div align=center>差异</div>";
		vcGrid.hdr.rows[2].cells[10].innerHTML = "<div align=center>版本一</div>";
		vcGrid.hdr.rows[2].cells[11].innerHTML = "<div align=center>版本二</div>";
		vcGrid.hdr.rows[2].cells[12].innerHTML = "<div align=center>差异</div>";
    }
    
	var checkProjectIds = checkProjectId.split(",");
	var projectId= checkProjectIds[0];
	var quoteProjectId = checkProjectIds[1];
	if(projectId == quoteProjectId){
		showMessage(false, "项目版本比较", "请选择两个版本进行比较");
		return;
	}
	var result = dhtmlxAjax.postSync(conextPath + "/project/projectAction!versionCompare.action?projectDto.project.id=" + projectId + "&projectDto.project.realProjectId=" + quoteProjectId, "").xmlDoc.responseText;
	if(result != "failed" && result != ""){
		if(result == "noVersionLogs"){$("versionCompareText").innerHTML = "没有变更历史 无法比较版本";return;}
		var results = result.split("$");
		projectId = results[0];
		quoteProjectId = results[1];
		parseVersionCompareDatas(vcGrid, results[2], 0);
		$("versionMessage").innerHTML = "版本一:" + projectGrid.cells(projectId,2).getValue() + "	&nbsp;&nbsp;&nbsp;版本二:" + projectGrid.cells(quoteProjectId,2).getValue();
		versonCompareW_ch.setDimension(995, $("versionCompareWin").offsetHeight + 30);
	}else{
		$("versionCompareText").innerHTML = "无法比较版本";
	}
}

function parseCustomDatas(grid, results){
	grid.clearAll();
	if(results != ""){
		jsons = eval("(" + results +")");
		grid.parse(jsons, "json");
		var allItems = grid.getAllItemIds();
		if(allItems == "")return;
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			grid.cells(items[i],1).setValue(i + 1);
			grid.cells(items[i],5).setValue(precddenceArray[grid.cells(items[i],5).getValue()]);
			grid.cells(items[i],10).setValue(planFlagArray[grid.cells(items[i],10).getValue()]);
		}
	}
}

function parseVersionCompareDatas(grid, results, index){
	grid.clearAll();
	if(results != ""){
		jsons = eval("(" + results +")");
		grid.parse(jsons, "json");
		var allItems = grid.getAllItemIds();
		if(allItems == "")return;
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			grid.cells(items[i],index).setValue(i + 1);
			var compareFlag = grid.cells(items[i],1).getValue();
			if(compareFlag == 1){
				grid.cells(items[i],1).setValue("<img src=" + imagePath + "1.gif /></div>");
			}else if(compareFlag == -1){
				grid.cells(items[i],1).setValue("<img src=" + imagePath + "-1.gif />");
			}else{
				grid.cells(items[i],1).setValue("");
			}
			var col10s = grid.cells(items[i],16).getValue().split("-");
			var padding = getPadding(col10s[1]);
			grid.setCellTextStyle(items[i],2,"padding-left:" + padding);
			compareResource(grid, items[i]);
		}
	}
}

function getPadding(level){
	return 10 * parseInt(level);
}

function getisDigit(number){
	if(number == "")return;
	var c = Math.floor(number);
	var a = c * 1000;
	var b = number * 1000;
	if(a == b)return c; else return number;
}

function compareResource(grid, rowId){
	var t1 = grid.cells(rowId,3).getValue();
	var t2 = grid.cells(rowId,4).getValue();
	var t3 = grid.cells(rowId,5).getValue();
	var t4 = grid.cells(rowId,6).getValue();
	if(t1 != t2){
		grid.setCellTextStyle(rowId, 3, 'color:red;');
		grid.setCellTextStyle(rowId, 5, 'color:red;');
	}
	if(t3 != t4){
		grid.setCellTextStyle(rowId, 4, 'color:red;');
		grid.setCellTextStyle(rowId, 6, 'color:red;');
	}
	var r1 = grid.cells(rowId,13).getValue();
	var r2 = grid.cells(rowId,14).getValue();
	if(r1 == r2){
		if(r1 == ""){
			grid.cells(rowId,15).setValue("");
		}else{
			grid.cells(rowId,15).setValue("相同");
		}
	}else{
		if(r1 == ""){
			grid.cells(rowId,15).setValue("不同");
			grid.setCellTextStyle(rowId, 15, 'color:red;')
		}else{
			grid.cells(rowId,15).setValue("相同");
			var r1s = r1.split("|");
			var resource, loginId, flag = true;
			for(var i = 0; i < r1s.length; i++){
				resource = trim(r1s[i]);
				if(resource.indexOf("(") == -1)loginId = resource;
				else loginId = resource.substring(0, resource.indexOf("("));
				if(r2.indexOf(loginId) == -1){
					grid.cells(rowId,15).setValue("不同");
					grid.setCellTextStyle(rowId, 15, 'color:red;');
					flag = false;
					break;
				}
				if(flag){
					r1s = r2.split("|");
					resource = trim(r1s[i]);
					if(resource.indexOf("(") == -1)loginId = resource;
					else loginId = resource.substring(0, resource.indexOf("("));
					if(r1.indexOf(loginId) == -1){
						grid.cells(rowId,15).setValue("不同");
						grid.setCellTextStyle(rowId, 15, 'color:red;');
						flag = false;
						break;
					}
				}
			}
		}
	}
}
</script>