<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>反馈管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY  style="overflow-x:hidden;overflow-y:hidden;">
	<script type="text/javascript">
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="activeForm"name="activeForm" namespace="" action="">
				<input type="hidden" id="operCmd" name="dto.operCmd"value="regUserLicense" />
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="720">
					<tr class="ev_mypm"> 
						<td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							机器码:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="machineCode" name="dto.dto.machineCode" cssClass="text_c" 
								cssStyle="width:640;padding:2 0 0 4;" maxlength="100"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							激活码:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="activeCode" name="dto.dto.activeCode" cssClass="text_c" 
								cssStyle="width:640;padding:2 0 0 4;" maxlength="100"></ww:textfield>
						</td>
					</tr>
					<tr  class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0" nowrap>
							说明:
						</td>
						<td class="tdtxt" colspan="5"style="background-color: #f7f7f7;  width: 640;border-right:0">
							<ww:textarea id="remark"  readonly="true" name="remark" cssClass="text_c" cssStyle="width:640;padding:2 0 0 4;" rows="8"></ww:textarea>
						</td>
					</tr>
					<tr  class="ev_mypm">
					<td class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
					  <a class="bluebtn" href="javascript:void(0);"onclick="parent.parent.ulActiveW_ch.setModal(false);parent.ulActiveW_ch.hide();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="setActiveCode();"><span>确定</span> </a>
					<td>
					</tr>
			</table>
		  </ww:form>
		</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<script type="text/javascript">
	var remark = "本激活码仅授板在当前所安装MYPM上使用。";
	remark = remark +"激活后，所获得的在线用户数，无时间和项目数限制，且在以后的免费版升级版本中，将一直保有己购买的在线用户数。";
	remark = remark +"如给我们积极的反馈或建议，将视情况奖励一定在线用户数，并在MYPM公告栏中公示所提建议或反馈。";
	document.getElementById("remark").value=remark;
	function setActiveCode(){
		if(isWhitespace($("machineCode").value)){
			$("cUMTxt").innerHTML="机器码不能为空";
			$("machineCode").focus();
			return ;
		}
		if(isWhitespace($("activeCode").value)){
			$("cUMTxt").innerHTML="激活码不能为空";
			$("activeCode").focus();
			return ;
		}
		var url = conextPath+"/commonAction!userLicenseMgr.action";
		var rest = dhtmlxAjax.postSync(url,"activeForm","noJsonPreFlg").xmlDoc.responseText; 
		if(rest.indexOf("success")>=0)	{
			$("cUMTxt").innerHTML=rest.split("^")[1];
			//$("activeForm").reset();
		}else if(rest = "invalidA"){
			$("cUMTxt").innerHTML="无效的激活码";
		}else if(rest = "invalidM"){
			$("cUMTxt").innerHTML="无效的机器码";
		}else if(rest = "failed"){
			$("cUMTxt").innerHTML="激活失败";
		}else if(rest = "invalid"){
			$("cUMTxt").innerHTML="无效的激活数据";
		}else if(rest = "wrError"){
			$("cUMTxt").innerHTML="激活发生错误";
		}
	}

	</script>
	</BODY>

</HTML>
