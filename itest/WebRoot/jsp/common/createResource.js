	var cuW_ch, batchCW_ch;
	function cuRe(id){
		cuW_ch = initWch(cuW_ch, "createResource", false, 390, 255, "", "新建资源");
		formResetAll();setUserResourceEnable(false);if(id=="newResource")$3("resource_id").value = "";else $3("resource_id").value = pmGrid.getSelectedId();
		$3("createResourceText").innerHTML = "&nbsp;";
		$3("displayName").focus();
		var customTaskHomeId = "";
		if(typeof ($3("customTaskHomeId") != "undefined"))
			customTaskHomeId = $3("customTaskHomeId").value;
		
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/helper/helperAction!findResource.action?helperDto.resource.id=" + $3("resource_id").value + "&helperDto.projectId="+customTaskHomeId, "");
		if(mypmDoc == "")return;
		var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			if(id=="updateResource" && result == "noUpdate"){
				cuW_ch.hide();
				showMessage(false, "修改资源", "资源已被任务引用 不能修改");
				return;
			}
		}else{
			showMessage(false, "资源操作", "加载数据失败");
			return;
		}
		var results = result.split("$");
		if(results[0] == "1")
			$3("temporary").checked = true;
		else
			$3("temporary").checked = false;
		if(id=="newResource"){
			$3("resource_id").value = "";
			$3("cuR_b").title = "创建";
		}else if(id=="updateResource"){
			$3("cuR_b").title = "修改";
			cuW_ch.setText("修改资源");
			if(results[1] != ""){
				var subItems = results[1].split("^");
				$3("resource_id").value = subItems[0];
				$3("displayName").value = subItems[1];
				$3("scalar").value = getisDigit(subItems[2]);
				$3("scalarUnit").value = subItems[3];
				$3("remark").value = subItems[4];
				$3("resourceType" + subItems[5]).checked = true;
				if(subItems[5] == 0){
					setUserResourceEnable(false);
					$3("headship").value = subItems[6];
					var overTime = getisDigit(subItems[7]);
					$3("overTime").value = (overTime == 0) ? "" : overTime;
					$3("startDate").value = subItems[8];
					$3("endDate").value = subItems[9];
					$3("userId").value = subItems[10];
				}else{
					setUserResourceEnable(true);
				}
			}else{
				$3("createResourceText").innerHTML = "没有加载到数据";
			}
		}
		initDhtmlxCalendar();
	}
	
	function createOrUpdateResource(){
		if(isWhitespace($3("displayName").value)){
			$3("createResourceText").innerHTML = "请填写资源名称";
			$3("displayName").focus();
			return;
		}
		if($3("resourceType0").checked && isWhitespace($3("userId").value)){
			$3("createResourceText").innerHTML = "请选择资源关联的用户";
			$3("displayName").value = "";
			$3("displayName").focus();
			return;
		}
		if(isWhitespace($3("scalar").value) || !isFloat($3("scalar").value, false)){
			$3("createResourceText").innerHTML = "请填写资源数值";
			$3("scalar").focus();
			return;
		}
		if(parseFloat($3("scalar").value) < 0){
			$3("createResourceText").innerHTML = "请填写正确的资源数值";
			$3("scalar").focus();
			return;
		}
		if($3("resourceType0").checked && isWhitespace($3("startDate").value)){
			$3("createResourceText").innerHTML = "请选择资源开始可用时间";
			$3("startDate").focus();
			return;  
		}
		if($3("resourceType0").checked && !isWhitespace($3("endDate").value) && $3("startDate").value >= $3("endDate").value){
			$3("createResourceText").innerHTML = "资源开始可用时间必须在可用到时间之前";
			return;
		}
		if(!isFloat($3("overTime").value, true)){
			$3("createResourceText").innerHTML = "请填写正确的加班费率数值";
			$3("overTime").focus();
			return;
		}
		var msg, month;
		if($3("resource_id").value == ""){
			month = "createResource";
			msg = "创建";
		}else{
			month = "updateResource";
			msg = "修改";
		}
		var projectId = pmBar.getListOptionSelected("project");
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/helper/helperAction!" + month + ".action?helperDto.resource.projectId=" + projectId, "createResourceF");
		if(mypmDoc == "")return;
		var result = mypmDoc.xmlDoc.responseText;
		if($3("resourceFlagExt") != null){
			if(result != "failed" && result != ""){
				formResetAll2();
				$3("createResourceText").innerHTML = "创建资源成功";
			}else{
				$3("createResourceText").innerHTML = "创建资源失败";
			}
			return;
		}
		if(result != "failed" && result != ""){
			if(result == "userIdIsNull"){
				$3("createResourceText").innerHTML = "工时资源关联的用户不存在 请重新选择";
				return;
			}
			var results = result.split("^");
			var rowNum;
			if(month == "createResource"){
				rowNum = 0;
				pmGrid.addRow(results[0],results[1],0);
				redressPageSize(pmGrid);
			}else{
				rowNum = pmGrid.cells(results[0],1).getValue() - 1;
				pmGrid.deleteRow(results[0]);
				pmGrid._addRow(results[0],results[1],rowNum);
			}
			setDigit(results[0]);
			setCheckboxDisabled(pmGrid, 1, true);
			pmGrid.setSelectedRow(pmGrid.getRowId(rowNum));
			if(month == "updateResource")cuW_ch.hide();
			formResetAll2();
			$3("createResourceText").innerHTML = msg + "资源成功";
			pmGrid.setSizes();
		}else{
			$3("createResourceText").innerHTML = msg + "资源失败";
		}	
	}
	
	function setDigit(rowId){
		pmGrid.cells(rowId, 4).setValue(getisDigit(pmGrid.cells(rowId, 4).getValue()));
		pmGrid.cells(rowId, 9).setValue(getisDigit(pmGrid.cells(rowId, 9).getValue()));
	}
	
	function setResourceType(obj){
		if(obj.id == "resourceType0"){
			setUserResourceEnable(false);
		}else{
			setUserResourceEnable(true);
		}
	}
	
	function setUserResourceEnable(flag){
		if(flag){
			$3("headship").value = "";
			//$3("resourceStatus1").check = true;
			$3("overTime").value = "";
			$3("startDate").value = "";
			$3("endDate").value = "";
			$3("userId").value = "";
			$3("scalarLable").innerHTML = "";
			$3('startDateTD').style.color = '';
			$3("searchUserImg").style.display = "none";
			$3("displayName").style.width = "265px";
		}else{
			$3("scalarLable").innerHTML = "/月";
			$3('startDateTD').style.color = 'red';
			$3("searchUserImg").style.display = "";
			$3("displayName").style.width = "245px";
		}
		$3("headship").disabled = flag;
		$3("overTime").disabled = flag;
		$3("startDate").disabled = flag; 
		$3("endDate").disabled = flag;
	}
	
	function batchCreateResource(){
	  	if(isWhitespace($3("name_batch").value)){
			$3("createResourceText_batch").innerHTML = "请选择资源关联的用户";
			return;
		}
		if(isWhitespace($3("scalar_batch").value) || !isFloat($3("scalar_batch").value, false)){
			$3("createResourceText_batch").innerHTML = "请填写资源数值";
			$3("scalar_batch").focus();
			return;
		}
		if(parseFloat($3("scalar_batch").value) < 0){
			$3("createResourceText_batch").innerHTML = "请填写正确的资源数值";
			$3("scalar_batch").focus();
			return;
		}
		if(isWhitespace($3("startDate_batch").value)){
			$3("createResourceText_batch").innerHTML = "请选择资源开始可用时间";
			$3("startDate_batch").focus();
			return;  
		}
		if(!isWhitespace($3("endDate_batch").value) && $3("startDate_batch").value >= $3("endDate_batch").value){
			$3("createResourceText_batch").innerHTML = "资源开始可用时间必须在可用到时间之前";
			return;
		}
		if(!isFloat($3("overTime_batch").value, true)){
			$3("createResourceText_batch").innerHTML = "请填写正确的加班费率数值";
			$3("overTime_batch").focus();
			return;
		}
	  	var projectId = pmBar.getListOptionSelected("project");
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/helper/helperAction!batchCreateResource.action?helperDto.resource.projectId=" + projectId, "batchCreateResourceF");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if($3("resourceFlagExt") != null){
			if(result != "failed" && result != ""){
				formReset('batchCreateResourceF', 'name_batch');
				$3("createResourceText_batch").innerHTML = "创建资源成功";
			}else{
				$3("createResourceText_batch").innerHTML = "创建资源失败";
			}
			return;
		}
		if(result != "failed" && result != ""){
			 var results = result.split("$");
			 for(var i = 0; i < results.length; i++){
			 	if(results[i] == "")continue;
				var rowDatas = results[i].split("^");
			 	pmGrid.addRow(rowDatas[0], rowDatas[1], i);
			 	setDigit(rowDatas[0]);
			 }
			 setCheckboxDisabled(pmGrid, 1, true);
			 formReset('batchCreateResourceF', 'name_batch');
			 $3("createResourceText_batch").innerHTML = "创建资源成功";
		}else{
			$3("createResourceText_batch").innerHTML = "批量创建资源失败";
		}
  }
  
  function  batchCR(){
  	batchCW_ch = initWch(batchCW_ch, "batchCreateResourceDiv", false, 390, 183, "", "批量新建工时资源");
  	formReset("batchCreateResourceF");
  	$3("createResourceText_batch").innerHTML = "&nbsp;";
  	initDhtmlxCalendar();
  	var customTaskHomeId = "";
	if(typeof ($3("customTaskHomeId") != "undefined"))
		customTaskHomeId = $3("customTaskHomeId").value;
	var mypmDoc = dhtmlxAjax.postSync(conextPath + "/helper/helperAction!findResource.action?helperDto.resource.id=&helperDto.projectId="+customTaskHomeId, "");
	if(mypmDoc == "")return;
	var result = mypmDoc.xmlDoc.responseText;
	var results = result.split("$");
	if(results[0] == "1")
		$3("temporary_batch").checked = true;
	else
		$3("temporary_batch").checked = false;
  }
  
  function searchUser(){
	if($3("resourceType0").checked){
		popselWin("userId", "displayName",',',false,1);
	}
  }
  
  function searchUser_batch(){
	popselWin("userId_batch", "name_batch",',',false,1000);
  }
  
  function formResetAll(){
  	formReset('createResourceF', 'displayName');
  	$3("resource_id").value = "";
  	$3("userId").value = "";
  }
  
  function formResetAll2(){
  	$3("displayName").value = "";
  	$3("scalar").value = "";
  	$3("overTime").value = "";
  	$3("headship").value = "";
  	$3("remark").value = "";
  	$3("resource_id").value = "";
  	$3("userId").value = "";
  }