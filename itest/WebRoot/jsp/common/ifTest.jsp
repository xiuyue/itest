<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>

<HTML>
<HEAD>
	<TITLE>MYPM项目管理及测试平台</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/menu/codebase/skins/dhtmlxmenu_dhx_blue.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/menu/codebase/dhtmlxmenu.js"></script>
</HEAD>
<BODY style="overflow-y:hidden;">
 <script language="Javascript">  
	 function dyniframesize(iframename) {  
	 //alert(this.document.body.scrollHeight); 
	 var obj = parent.document.getElementById("mypmMain");  
	 //alert(obj.height);
	 obj.height = this.document.body.scrollHeight;
 }  
 </script> 
 <div id="logoDiv">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout: fixed;">
		<tr>
			<td height="55" background="<%=request.getContextPath()%>/jsp/common/images/temp/bg_sampleexplorer_header.gif" width="286" valign="top">
				<img height="55" border="0" width="256" src="<%=request.getContextPath()%>/jsp/common/images/temp/logo_sampleexplorer.gif"/>
			</td>
			<td height="55" background="<%=request.getContextPath()%>/jsp/common/images/temp/bg_sampleexplorer_header.gif" align="right" valign="top"/>
			<td height="55" background="<%=request.getContextPath()%>/jsp/common/images/temp/bg_sampleexplorer_header.gif" align="right" valign="top">
				<img height="55" width="302" src="<%=request.getContextPath()%>/jsp/common/images/temp/title_sampleexplorer.gif"/>
			</td>
		</tr>
	</table>
</div>
<div id="menuDiv" style="background-color:#D3E2E5;height:25px;">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" style="table-layout: fixed;">
		<tr>
			<td>
			<div id="menuObj"></div>
	<script type="text/javascript">
	</script>
			</td>
			<td align="right" style="padding-top:3px;">
				<img id="upImg" src="<%=request.getContextPath()%>/jsp/common/images/up.gif" title="隐藏logo" onclick="upLogo();" />
				<img id="downImg" src="<%=request.getContextPath()%>/jsp/common/images/down.gif" title="显示logo" onclick="downLogo();" style="display:none;" />
			</td>
		</tr>
	</table>
</div>
<div style="width:100%">
		  	<iframe id="mypmMain" onload="javascript:{dyniframesize('mypmMain');}" name="mypmMain" src="http://127.0.0.1:8080/mypm/userManager/userManagerAction!userList.action" frameborder=0 scrolling="no" width="100%"  style="margin:0px;"></iframe>
</div>

</BODY>
	<script type="text/javascript">
	var _isFF = false;
	var _isIE = false;
	var _isOpera = false;
	var _isKHTML = false;
	var _isMacOS = false;
	if (navigator.userAgent.indexOf('Macintosh') != -1) 
	    _isMacOS = true;
	if ((navigator.userAgent.indexOf('Safari') != -1) || (navigator.userAgent.indexOf('Konqueror') != -1)) {
	    var _KHTMLrv = parseFloat(navigator.userAgent.substr(navigator.userAgent.indexOf('Safari') + 7, 5));
	    if (_KHTMLrv > 525) {
	        _isFF = true;
	        var _FFrv = 1.9
	    }
	    else 
	        _isKHTML = true
	}
	else 
	    if (navigator.userAgent.indexOf('Opera') != -1) {
	        _isOpera = true;
	        _OperaRv = parseFloat(navigator.userAgent.substr(navigator.userAgent.indexOf('Opera') + 6, 3))
	    }
	    else 
	        if (navigator.appName.indexOf("Microsoft") != -1) {
	            _isIE = true;
	            if (navigator.appVersion.indexOf("MSIE 8.0") != -1 && document.compatMode != "BackCompat") 
	                _isIE = 8
	        }
	        else {
	            _isFF = true;
	            var _FFrv = parseFloat(navigator.userAgent.split("rv:")[1])
	        };
	        
	//resizeFrameset(true);
	document.getElementById("logoDiv").style.width = document.body.clientWidth-21;
	document.getElementById("menuDiv").style.width = document.body.clientWidth-21;
	
	function upLogo(){
		document.getElementById("logoDiv").style.display = "none";
		document.getElementById("upImg").style.display = "none";
		document.getElementById("downImg").style.display = "";
		resizeFrameset(false);
	}
	
	function downLogo(){
		document.getElementById("logoDiv").style.display = "";
		document.getElementById("upImg").style.display = "";
		document.getElementById("downImg").style.display = "none";
		resizeFrameset(true);
	}
	
	function resizeFrameset(flag) {
		dyniframesize('mypmMain');
	}
</script>
</html>