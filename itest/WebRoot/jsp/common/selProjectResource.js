	var proResourceW_ch, proResourceG, seledProResourceG, projectStatus, projectPlanFlag;
	function selProResources(projectId){
		if(projectId ==null || projectId == ""){
			if(typeof proResourceW_ch != "undefined"){
				proResourceW_ch.hide();
			}
			showMessage(false, "设置项目公用资源", "请选择项目");
			return;
		}
		$("setProjectId").value = projectId;
		var width = (_isFF) ?  409: 404;
		proResourceW_ch = initW_ch(proResourceW_ch,  "selProjectResourceDiv", false, width, 190, "", "设置项目公用资源");
		$("selProjectResourceText").innerHTML = "提示:项目周期内都使用的资源, 如办公场地费用等, 与任务无关";
		if(typeof proResourceG == "undefined"){
			proResourceG = new dhtmlXGridObject('selGridbox_proR');
			proResourceG.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
			proResourceG.setHeader("<div title='双击行数据 可以选择资源'>&nbsp;</div>,<div title='双击行数据 可以选择资源'>备选资源</div");
			proResourceG.setInitWidths("0,*");
			proResourceG.setColAlign("center,left");
			proResourceG.setColTypes("ro,ed");
			proResourceG.setColSorting("int,str");
			proResourceG.attachEvent("onRowDblClicked",onRowDblClicked_proR);
			proResourceG.enableAutoHeight(true, 120);
			proResourceG.init();
			proResourceG.setSkin("light");
			seledProResourceG = new dhtmlXGridObject('seledGridbox_proR');
			seledProResourceG.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
			seledProResourceG.setHeader("&nbsp;,&nbsp;,<div title='双击行数据 可以删除资源'>已选资源</div>,&nbsp;");
			seledProResourceG.setInitWidths("0,0,*,0");
			seledProResourceG.setColAlign("center,center,left,left");
			seledProResourceG.setColTypes("ro,ro,ed,ro");
			seledProResourceG.setColSorting("int,int,str,int");
			seledProResourceG.attachEvent("onRowDblClicked",seledonRowDblClicked_proR);
			seledProResourceG.enableAutoHeight(true, 120);
			seledProResourceG.init();
			seledProResourceG.setSkin("light");
	    }
	    var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!setProjectResourceInit.action?taskDto.task.projectId=" + projectId, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			seledProResourceG.clearAll();
			proResourceG.clearAll();
			var results = result.split("$");
			if(results[0] != ""){
				seledProResourceG.parse(eval("(" + results[0] +")"), "json");
				setRowNumIndexById(seledProResourceG, 0);
			}
			if(results[1] != ""){
				proResourceG.parse(eval("(" + results[1] +")"), "json");
				setRowNumIndexById(proResourceG, 0);
			}
			if(results[2] == "0"){
				$("selProjectResourceImg").style.display = "none";
			}else{
				$("selProjectResourceImg").style.display = "";
			}
			projectStatus = results[2];
			projectPlanFlag = results[3];
			resetWinHeight(proResourceW_ch, "selProjectResourceDiv", width, 190);
		}else{
			showMessage(false, "设置项目公用资源", "没有项目资源");
		}
	}
	
	function onRowDblClicked_proR(rowId,cellInd,state){
		var allItems = seledProResourceG.getAllItemIds();
		if(allItems == "" || allItems.indexOf(rowId) == -1){
			seledProResourceG.addRow(rowId, "0,0," + proResourceG.cells(rowId,1).getValue() + "," + projectPlanFlag, 0);
			seledProResourceG.setSizes();
			proResourceG.deleteRow(rowId);
			proResourceG.setSizes();
		}else{
			$("selProjectResourceText").innerHTML = "不能添加重复的资源";
		}
		return false;
	}
	
	function seledonRowDblClicked_proR(rowId,cellInd,state){
		if(projectStatus == "0"){$("selProjectResourceText").innerHTML = "项目未被激活 不能设置公用资源";return;}
		if(projectPlanFlag == "0" || (projectPlanFlag == "1" && seledProResourceG.cells(rowId,3).getValue() == 1)){
			proResourceG.addRow(rowId, "0," + seledProResourceG.cells(rowId,2).getValue(), 0);
			proResourceG.setSizes();
			seledProResourceG.deleteRow(rowId);
			seledProResourceG.setSizes();
		}else{
			$("selProjectResourceText").innerHTML = "选中的资源在项目计划中已经确定 不能删除";
		}
		return false;
	}
	
	function selProjectResource(){
		var allItems = seledProResourceG.getAllItemIds();
		//if(allItems == ""){
			//$("selProjectResourceText").innerHTML = "请选择项目资源";
			//return;
		//}
		var projectId = $("setProjectId").value;
		var mypmDoc = dhtmlxAjax.postSync(conextPath + "/task/taskAction!setProjectResource.action?taskDto.task.projectId=" + projectId + "&taskDto.selectStr=" + allItems, "");
	  	if(mypmDoc == "")return;
	  	var result = mypmDoc.xmlDoc.responseText;
		if(result != "failed" && result != ""){
			if(result == "noPermision"){$3("selProjectResourceText").innerHTML = "此项目您没有权限操作";return;}
			if(result == "lockProject"){$3("selProjectResourceText").innerHTML = "已经确定计划 不能修改";return;}
			if(result == "noDatas"){$3("selProjectResourceText").innerHTML = "设置项目公用资源成功 项目中已经没有公用资源";return;}
			proResourceW_ch.hide();
			showMessage(false, "设置项目公用资源", "设置项目公用资源成功");
		}else{
			$("selProjectResourceText").innerHTML = "设置项目公用资源失败";
		}
	}