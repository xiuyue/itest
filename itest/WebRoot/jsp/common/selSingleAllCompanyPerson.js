	var callBackCustom2;
	var initGroypId2="-1";
	var peopleId2 ;
	var peopleName ;
	var comUserSelW_ch2;
	var selGrid2;
	var initPassValId = "";
	function popselWinInAll(passValId,passNameId,model,callBack){
		if(typeof callBack == "undefined")
			callBackCustom2 = "";
		else
			callBackCustom2 = callBack;
		peopleId2 = passValId;
		peopleName = passNameId;
		if(typeof comUserSelW_ch2 !="undefined"&&comUserSelW_ch2.isHidden()){
			if(typeof(model) !="undefined"){
				comUserSelW_ch2.setModal(model);
			}else{
				comUserSelW_ch2.setModal(true);
			}
			markSel2();
			comUserSelW_ch2.show();
			comUserSelW_ch2.bringToTop();
			return;
		}
		if(typeof(importWinJs) != "undefined")
			importWinJs();
		if(typeof(model) !="undefined"){
			if(_isFF)
				comUserSelW_ch2 = initW_ch(comUserSelW_ch2, "selPeopleDiv2", true, 190, 380);
			else
			    comUserSelW_ch2 = initW_ch(comUserSelW_ch2, "selPeopleDiv2", true, 190, 360);
		}else{
			if(_isFF)
				comUserSelW_ch2 = initW_ch(comUserSelW_ch2, "selPeopleDiv2", true, 190, 380);
			else
			    comUserSelW_ch2 = initW_ch(comUserSelW_ch2, "selPeopleDiv2", true, 190, 360);
			
			comUserSelW_ch2.setModal(true);
		}
		if(typeof(model) =="undefined")
			comUserSelW_ch2.button("close").attachEvent("onClick", function(){
				comUserSelW_ch2.setModal(false);
				comUserSelW_ch2.hide();
			});
		else
			comUserSelW_ch2.button("close").attachEvent("onClick", function(){
				comUserSelW_ch2.hide();
			});
		comUserSelW_ch2.setText("选择人员");
		initSelGrid2();//初始化grid
		initGroup2("sel_groupIds2");//初始化组下拦框
		initSleEdPeople2(passValId,passNameId);//初始化grid数据
		comUserSelW_ch2.bringToTop();
	}

	
	function initSleEdPeople2(passValId,passNameId){
		var seledPeopeId = $(passValId).value;
		var seledPeopeName = $(passNameId).value;
		if(seledPeopeId==""||seledPeopeName==""){
			return;
		}
		if(selGrid2.getRowIndex(seledPeopeId)>0){
			selGrid2.setRowColor(seledPeopeId, "#CCCCCC");
		}	
	}
	
	function initGroup2(id){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		$(id).options.length = 1;
		if(groupSel != ""){
			var options = groupSel.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					var selable = options[i].split(";")[1];
					$(id).options.add(new Option(selable,selvalue));
			}
			if(initGroypId2!="-1"){
				$("sel_groupIds2").value=initGroypId2;
			}
		}
	}

	function loadSelUserInAll(isDefaultLoad){
		initGroypId2 = $3("sel_groupIds2").value;
		var url = conextPath+"/userManager/userManagerAction!selectUserInAll.action?dto.isAjax=true";
		if(typeof isDefaultLoad != "undefined"){
			url = conextPath+"/userManager/userManagerAction!loadDefaultSelUserInAll.action?dto.isAjax=true";
		}
		var ajaxResut  = dhtmlxAjax.postSync(url, "selPeopleForm2").xmlDoc.responseText;
		if(ajaxResut=="failed"){
  			if(typeof hintMsg!="undefined"){
  				hintMsg("加载数据发生错误");
  			}else{
  				showMessage(true, "选择人员", "未加载到数据");
  			}
  			return;
  		}
		selGrid2.clearAll();
		ajaxResut = ajaxResut.split("$")[1];
	    if(ajaxResut != ""&&ajaxResut!="failed"){
   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
   			initData = ajaxResut;
  			selGrid2.parse(eval("(" + ajaxResut +")"), "json");
  		}
		markSel2('mv');
		return;
	}
	function markSel2(mv){
		var seledPeopeId = $(peopleId2).value;
		if(seledPeopeId==""){
			return;
		}
 		var allItems = selGrid2.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			selGrid2.setRowColor(items[i], "#FFFFFF");
		}
		if(selGrid2.getRowIndex(seledPeopeId)>=0){
			if(typeof(mv) != "undefined"){
				var userName = selGrid2.cells(seledPeopeId,2).getValue();
				selGrid2.deleteRow(seledPeopeId);
				selGrid2.addRow(seledPeopeId,'0,,'+userName,0);
				selGrid2.setRowColor(seledPeopeId, "#CCCCCC");
			}else{
				selGrid2.setRowColor(seledPeopeId, "#CCCCCC");
			}
		}			
	}
	function selPeople2(){
		if(selGrid2.getRowsNum()==0){
			comUserSelW_ch2.hide();
			comUserSelW_ch2.setModal(false);
			$(peopleId2).value = "";
			$(peopleName).value = "" ;
			return;
		}
		$(peopleId2).value = selGrid2.getSelectedId();
		$(peopleName).value =  selGrid2.cells($(peopleId2).value,2).getValue();
		comUserSelW_ch2.setModal(false);
		comUserSelW_ch2.hide();
		if(typeof(callBackCustom2) != "undefined" && callBackCustom2 != ""){
			eval(callBackCustom2);
		}
	}
	
	function initSelGrid2(){
	    if(typeof selGrid2 == "undefined"){
		    selGrid2 = new dhtmlXGridObject('selGridbox2');
			selGrid2.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid2.setHeader(",,<div title='双击数据行选择'>备选人员 -- 双击选择</div>");
			selGrid2.setInitWidths("0,0,*");
	    	selGrid2.setColAlign("left,left,left");
	    	selGrid2.setColTypes("ro,ro,ro");
	   		selGrid2.setColSorting("str,int,str");
	   		selGrid2.enableTooltips("false,false,true");
	    	selGrid2.enableAutoHeight(true, 240);
	    	selGrid2.setMultiselect(true);
	        selGrid2.init();
	        selGrid2.enableRowsHover(true, "red");
	    	selGrid2.setSkin("light");
	    	selGrid2.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
	    		selPeople2();
			});
			loadSelUserInAll('default');
	    }
	}