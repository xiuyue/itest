	var callBackCustom;
	var peopleId ;
	var peopleName ;
	var testTaskSelGrid;
	var testTaskUserSelW_ch,testMembers="";
	function popTeamMEMselWin(passValId,passNameId,model,callBack){
		if(typeof callBack == "undefined")
			callBackCustom = "";
		else
			callBackCustom = callBack;
		peopleId = passValId;
		peopleName = passNameId;
		if(typeof testTaskUserSelW_ch !="undefined"&&testTaskUserSelW_ch.isHidden()){
			if(typeof(model) !="undefined"){
				testTaskUserSelW_ch.setModal(model);
			}else{
				testTaskUserSelW_ch.setModal(true);
			}
			testTaskSelGrid.clearAll();
			testTaskSelGrid.parse(eval("(" + testMembers +")"), "json");
			testTaskSelGrid.addRow("-1","0,,----所有------",0);
			markSel('mv');
			testTaskUserSelW_ch.show();
			testTaskUserSelW_ch.bringToTop();
			return;
		}
		if(_isFF)
			testTaskUserSelW_ch = initW_ch(testTaskUserSelW_ch, "testTaskSelPeopleDiv", true, 210, 360);
		else
		    testTaskUserSelW_ch = initW_ch(testTaskUserSelW_ch, "testTaskSelPeopleDiv", true, 210, 340);
		if(typeof(importWinJs) != "undefined")
			importWinJs();
		if(typeof(model) !="undefined"){
			testTaskUserSelW_ch.setModal(true);
		}
		if(typeof(model) =="undefined")
			testTaskUserSelW_ch.button("close").attachEvent("onClick", function(){
				testTaskUserSelW_ch.setModal(false);
				testTaskUserSelW_ch.hide();
			});
		else
			testTaskUserSelW_ch.button("close").attachEvent("onClick", function(){
				testTaskUserSelW_ch.hide();
			});
		testTaskUserSelW_ch.setText("选择人员");
		initSelGrid();//初始化grid
		testTaskUserSelW_ch.bringToTop();
	}

	function loadTestTaskSelUser(){
		var url = conextPath+"/outLineManager/outLineAction!selectAllEnableMember.action";
		var ajaxResut  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(ajaxResut=="failed"){
  			if(typeof hintMsg!="undefined"){
  				hintMsg("加载数据发生错误");
  			}else{
  				showMessage(true, "选择人员", "未加载到数据");
  			}
  			return;
  		}
		testTaskSelGrid.clearAll();
		ajaxResut = ajaxResut.split("$")[1];
	    if(ajaxResut != ""&&ajaxResut!="failed"){
   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
   			testMembers = ajaxResut;
  			testTaskSelGrid.parse(eval("(" + ajaxResut +")"), "json");
  		}
  		testTaskSelGrid.addRow("-1","0,,----所有------",0);
		markSel('mv');
		return;
	}
	function markSel(mv){
		var seledPeopeId = $(peopleId).value;
		if(seledPeopeId==""){
			return;
		}
 		var allItems = testTaskSelGrid.getAllItemIds();
		var items = allItems.split(',');

		if(testTaskSelGrid.getRowIndex(seledPeopeId)>=0){
			if(typeof(mv) != "undefined"){
				var userName = testTaskSelGrid.cells(seledPeopeId,2).getValue();
				testTaskSelGrid.deleteRow(seledPeopeId);
				testTaskSelGrid.addRow(seledPeopeId,'0,,'+userName,1);
				testTaskSelGrid.setRowColor(seledPeopeId, "#CCCCCC");
			}else{
				testTaskSelGrid.setRowColor(seledPeopeId, "#CCCCCC");
			}
		}			
	}
	function selPeople(){
		if(testTaskSelGrid.getRowsNum()==0){
			testTaskUserSelW_ch.hide();
			testTaskUserSelW_ch.setModal(false);
			$(peopleId).value = "";
			$(peopleName).value = "" ;
			return;
		}
		$(peopleId).value = testTaskSelGrid.getSelectedId();
		if($(peopleId).value!="-1")
			$(peopleName).value =  testTaskSelGrid.cells($(peopleId).value,2).getValue();
		else
			$(peopleName).value="";
		testTaskUserSelW_ch.setModal(false);
		testTaskUserSelW_ch.hide();
		if(typeof(callBackCustom) != "undefined" && callBackCustom != ""){
			eval(callBackCustom);
		}
	}
	
	function initSelGrid(){
	    if(typeof testTaskSelGrid == "undefined"){
		    testTaskSelGrid = new dhtmlXGridObject('testTaskSelGridbox');
			testTaskSelGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			testTaskSelGrid.setHeader(",,<div title='单击选择'></div>");
			testTaskSelGrid.setInitWidths("0,0,*");
	    	testTaskSelGrid.setColAlign("left,left,left");
	    	testTaskSelGrid.setColTypes("ro,ro,ro");
	   		testTaskSelGrid.setColSorting("str,int,str");
	   		testTaskSelGrid.enableTooltips("false,false,true");
	    	testTaskSelGrid.enableAutoHeight(true, 240);
	    	testTaskSelGrid.enableRowsHover(true, "red");
	        testTaskSelGrid.init();
	        testTaskSelGrid.enableRowsHover(true, "red");
	    	testTaskSelGrid.setSkin("light");
	    	testTaskSelGrid.attachEvent("onRowSelect",function(rowId,cellInd,state){
	    		selPeople();
			});
			loadTestTaskSelUser();
	    }
	}