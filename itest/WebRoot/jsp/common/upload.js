	var currTime,initFileBk="",fileChkMsg="",initImg="",newUpImg="",currFckObj;
	function getAttaInRichHtm(htmText){
		htmText = replaceAll(htmText,"<br />","");
		htmText = replaceAll(htmText,"<br />","");
		if(htmText==""||(htmText.indexOf("src")<0&&htmText.indexOf("<")<0&&htmText.indexOf("mypmUserFiles")<0&&htmText.indexOf("img")<0))
			return ""; 
		var initFiles="";
   		var conts = htmText.split("src");
   		for(var i=0; i<conts.length; i++){
       		if(conts[i].indexOf("/>")>0){//图片
       			var fileName = conts[i].split(" ")[0];
       			fileName = fileName.substring(fileName.lastIndexOf("/")+1,fileName.length-1);
       			initFiles+=" "+fileName;
       		}
   		}
		return initFiles;
	}
	function eraseAttach(eraseAllImg){
		var eraseFiles =getDelFiles(eraseAllImg);
		if(isWhitespace(eraseFiles))
			return;
		dhtmlxAjax.post(conextPath+"/fileUpload?cmd=delete&eraseFiles="+eraseFiles);
		upVarReset();
	}
	function upVarReset(){
		initFileBk="";
		fileChkMsg="";
		initImg="";
		newUpImg="";
		$("currUpFile").value="";
		if(_isIE)
		    $('currUpFile').outerHTML=$('currUpFile').outerHTML;
		$("currAttach").style.display="none";
		$('upStatusBar').innerHTML="";
		$("attachUrl").value="";
	}
	function getDelFiles(eraseAllImg){
		if(typeof(eraseAllImg)!="undefined")//不提交关闭窗
			return newUpImg;
		if(newUpImg==""&&initImg==""&&initFileBk=="")
			return "";
		var upImgs = (initImg+newUpImg).split(" ");
		var currImg = "";
		if(typeof(currFckObj)!="undefined")
			currImg = getAttaInRichHtm(currFckObj.GetXHTML());
		if(currImg==""&&initFileBk==""){
			return initImg+newUpImg;
		}else if(currImg==""&&initFileBk!=""){
			return initImg+newUpImg+" "+initFileBk;
		}else{
			var currImgs = currImg.split(" ");
			var delFiles = initFileBk;
			for(var i=0; i<upImgs.length; i++){
				var matchFlg=0;
				for(var l=0;l<currImgs.length;l++){
					if(upImgs[i]==currImgs[l]){
						matchFlg=1;
						break;
					}
				}
				if(matchFlg==0)
				    delFiles+=" "+upImgs[i];
			}
			return delFiles;
		}
	}
	var loadStatus = "",instImg=0,upMsg="",upErrorCount=0,saveBtnId="saveBtn",subFun="";
	var subChkMeth="";
	function upLoadAndSub(sunFunction,chkMethod,instFckImg,richTextObj,formId){
		loadStatus = "loadIng";
		var currForm = "fileform";
		if(typeof(formId)!= "undefined"){
			currForm = formId;
		}
		upMsg="";
		if($("currUpFile").value==""&&typeof(instFckImg)!= "undefined"&&instFckImg!=1){
			eval(sunFunction+"()");
		}else{
			if(includeSpeChar(getFileName())){
				$('upStatusBar').innerHTML="文件名不能含特殊字符";
				return;
			}
			subFun = sunFunction;
			subChkMeth = chkMethod;
			if(eval(subChkMeth+"()")){
				instImg=0;
				initFileBk="";
				if(typeof(instFckImg)!= "undefined"&&instFckImg==1){
					instImg=1;
					if(typeof(richTextObj)!= "undefined")
						currFckObj = richTextObj;
				}
				$('upStatusBar').innerHTML="";
				currTime = (new Date()).getTime();
				if(fileChk(currForm)){
					$(currForm).action=conextPath+"/fileUpload?cmd=upload&upTime="+currTime;
					$(currForm).target="target_upload";
					$(currForm).submit();
					loadStatus = "loadIng";
					$(saveBtnId).style.display="none";
					$('upStatusBar').innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:5%\"></div></div>";
					statusChk();					
				}else{
					$("upStatusBar").innerHTML = "<div style='font-size: 12px;padding: 2 0 0 4;'><font color='Red'>"+fileChkMsg+"</font></div>";
					return;
				}
			}
		}
	}
	function statusChk(){
		var upPercent = "0";
	 	var ajaxRes = "";
	 	var waitCount = 1;//某些浏览器比如 chrome就很怪,先执行完statusChk再提交form,所以这里碰到这些浏览器时,像证性的检查三次就结束
	 	for(var i=0; i<2000;i++){//空循环是为了等上传一会以后再检查上传进度
		}
		$('upStatusBar').innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:5%\"></div></div>";
		while(loadStatus=="loadIng"){
			if(upMsg!=""){
				$('upStatusBar').innerHTML = "<div class='tdtxt'><font color='Red'>" + upMsg + "</font></div>";
				return;
			}
			if(upPercent=="error"){
				return;
			}
			var chkUrl = conextPath+"/fileUpload?cmd=chkUpLoad&upPercent="+upPercent+"&upTime="+currTime;
			if(chkUrl.indexOf("error")>=0){
				return;
			}
			ajaxRest = postSub(chkUrl,"");
			if(ajaxRest=="error"){
				 "<div style='width:200;padding:2 0 0 4;'><font color='Red'>上传发生错误</font></div>";
				 if(!_isFF)
				 	eraseUpFlag();
				 return;
			}else if(ajaxRest=="ManyUpOccur"){
				 "<div style='width:200;padding:2 0 0 4;'><font color='Red'>正在上传其他附件，稍后再试</font></div>";
				 return;			
			}
			//解决chrome的问题
			if(waitCount>=3&&upPercent=="0"){
				loadStatus = "error";
				return;
			}
			if(ajaxRest!=""&&ajaxRest.split("^")[0]!=upPercent){
				 var rest = ajaxRest.split("^")[1];
				 if(typeof rest!="undefined")
				 	$('upStatusBar').innerHTML = rest;
				if(ajaxRest.split("^")[0]=="100"){
				 if(!_isFF)
				 	eraseUpFlag();
					return;
				}
				if(upPercent=="0"||(ajaxRest!=""&&upPercent==ajaxRest.split("^")[0])){//上传进度不变，等一会再检查
					for(var l=0; l<20000;l++){
					}
				}
				upPercent= ajaxRest.split("^")[0];	
			}
			if(upPercent=="0"){
				waitCount ++;
			}
		}
	}
	function eraseUpFlag(){
			 dhtmlxAjax.post(conextPath+"/fileUpload?cmd=erase&upTime="+currTime);
	}
	
	function upMsgConv(msg){
		upMsg ="上传发生错误";
		if(msg=="ManyUpOccur"){
			upMsg="您正在上传其他附件，稍后再试";
		}else if(msg=="typeDeny"){
			upMsg="不能上传exe,bat,sh,jsp,js,htm,html,sql类型的文件";
		}else if(msg=="fileExceeds"){
			upMsg="单个文件不能超1M";
		}else if(msg=="Over5M"){
			upMsg="多个附件总计不能超5M";
		}	
		
	}
	function stopStatusChk(message,saveFileName){
		if(message!="finish"){
			loadStatus="error";
			upMsgConv(message);
			$('upStatusBar').innerHTML = "<div class='tdtxt'><font color='Red'>" + upMsg + "</font></div>";
			$(saveBtnId).style.display="";
			if(_isFF)
				eraseUpFlag();
			return;
		}
		loadStatus = "finish";
		$("upStatusBar").innerHTML ="<div class=\"prog-border\"><div class=\"prog-bar\" style=\"width:100%\"></div></div>";
		if(_isFF)
			eraseUpFlag();
		var upFiles=saveFileName.split(" ");
		if(instImg==0){
			initFileBk = $("attachUrl").value;
			$("attachUrl").value = upFiles[1];
			eval(subFun+"()");
			$("currUpFile").value="";
			if(_isIE)
		    	$('currUpFile').outerHTML=$('currUpFile').outerHTML;
		}else{
			//newUpImg+=" "+ upFiles[1]+"_"+getFileName();
			newUpImg+=" "+ upFiles[1];
			if(typeof(currFckObj)!="undefined")
				currFckObj.InsertHtml("<img src='"+upPath+upFiles[0]+"/"+upFiles[1]+"' style='width:300px;height:150px;'/>");
			$("currUpFile").value = "";
			if(_isIE)
		    	$('currUpFile').outerHTML=$('currUpFile').outerHTML;
		}
		$(saveBtnId).style.display="";
		saveBtnId="saveBtn";
	}
	function fileChk(currFormId){
		var allowType ="exe,EXE,BAT,bat,sh,SH,jsp,js,htm,html,sql";
		if(instImg==1){
			allowType="GIF,PNG,JPG,gif,png,jpg";
			fileChkMsg = "只能插入gif,pnp,jpg类型的图片";
		}else
			fileChkMsg = "不能上传exe,bat,sh,jsp,js,htm,html,sql类型的文件";
		var allowTypes = allowType.split(",");
		var myFormId = "fileform";
		if(typeof(currFormId)!= "undefined"){
			myFormId = currFormId;
		}
		var form = $(myFormId);
		var elements = form.elements;
		for (i = 0; i < elements.length; ++i) {
      		var element = elements[i];
      		if(element.type == "file"){
      			var fileName = element.value;
      			if(_isIE)//IE下取的是完整路径，要处理一下
      				fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
	      		if(fileName.indexOf(".")<0)
	      			return false;
	      		for(var l=0; l<allowTypes.length; l++){
	      			if(fileName.endWith("."+allowTypes[l])){
	      				if(instImg==1)
	      					return true;
	      				else
	      					return false;
	      			}
	      		}
	      		
	      	}
      	}
      	if(instImg==0)
      		return true;	
      	else
      	   	return false;	
	}
	function getFileName(){
		var fileName = $("currUpFile").value;
		if(_isIE){
			return fileName.substring(fileName.lastIndexOf("\\")+1);
		}else{
			return  fileName;
		}
	}
	
