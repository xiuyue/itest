	pageBreakUrl = conextPath+"/role/roleAction!roleList.action";
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,角色名,人员,备注,&nbsp;");
    pmGrid.setInitWidths("40,40,160,*,202,0");
    pmGrid.setColAlign("center,center,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str");
	pmGrid.attachEvent("OnCheck",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,false,true,true,true,true");
    pmGrid.setSkin("light");
   	pmGrid.attachEvent("onRowSelect",doOnSelect);
    if($("listStr").value != ""){
    	var data = $("listStr").value;
    	data = data.replace(/[\r\n]/g, "");
	    var datas = data.split("$");
	    if(datas[1] != ""){
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setPageNoSizeCount(datas[0]);
	    	setRowNum();
	    }
    }
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		resetBtn(pmGrid.cells(rowId,2).getValue());
	} 
    function doOnCheck(rowId,cellInd,state){//勾选
		pmGrid.setSelectedRow(rowId);
		resetBtn(pmGrid.cells(rowId,2).getValue());
		return true;
	}
	
	function resetBtn(roleName){
		if(roleName=="超级管理员") {
			try{
				pmBar.disableItem("delete");	
			}catch(err){}	
			try{
				pmBar.disableItem("broUser");	
			}catch(err){}	
			try{
				pmBar.disableItem("userMa");	
			}catch(err){}	
			try{
				pmBar.disableItem("broAu");
			}catch(err){}	
			try{
				pmBar.disableItem("authMa");
			}catch(err){}			
		}else{
			try{
				pmBar.enableItem("delete");	
			}catch(err){}	
			try{
				pmBar.enableItem("broUser");	
			}catch(err){}	
			try{
				pmBar.enableItem("userMa");	
			}catch(err){}	
			try{
				pmBar.enableItem("broAu");
			}catch(err){}	
			try{
				pmBar.enableItem("authMa");
			}catch(err){}			
		}

	}
    function setRowNum(){
    	var rowIdsToDel ="";
		for(var i = 0; i < pmGrid.getRowsNum(); i++){	
			pmGrid.cells2(i,1).setValue((i + 1)+((parseInt(pageNo)-1)*parseInt(pageSize)));
			if(i>(pageSize-1)){
				if(rowIdsToDel==""){
					rowIdsToDel = pmGrid.getRowId(i);
				}else{
					rowIdsToDel = rowIdsToDel +"," +pmGrid.getRowId(i);
				}
			}
		}
		if(rowIdsToDel !=""){
			var ids = rowIdsToDel.split(",");
			for(var l=0; l <ids.length; l++){
			 pmGrid.deleteRow(ids[l]);
			}
		}
    }
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			find("findD");
		}else if(id == "broUser"){
			broUser();
		}else if(id=="userMa"){
			userManager();
		}else if(id=="broAu"){
			broAuth();
		}else if(id=="authMa"){
			authManager();
		}else if(id == "new"){
			creater(id, "createD");
		}else if(id == 'update'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要修改的记录");
			}else{
				creater(id, "createD");
			}
		}else if(id == 'delete'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要删除的记录");
			}else{
				cfDialog("deleteExe","您确定删除选择的记录?",false);
			}
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	var broUW_ch;
	function broUser(){
		var roleId = pmGrid.getSelectedId();
		if(roleId== null){
			hintMsg("请选择角色");
			return;
		}
		var url = conextPath+"/role/roleAction!roleUserList.action?dto.role.roleId="+roleId;
		broUW_ch = initW_ch(broUW_ch, "", true, 835, 520,'broUW_ch');
		broUW_ch.attachURL(url);
		broUW_ch.setText("角色 "+pmGrid.cells(roleId,2).getValue()+":帐户列表");	
	    broUW_ch.show();
	    broUW_ch.bringToTop();
	}
	var useMaW_ch;
	function userManager(){
		var roleId = pmGrid.getSelectedId();
		if(roleId== null){
			hintMsg("请选择角色");
			return;
		}
		var url = conextPath+"/role/roleAction!userManager.action?dto.role.roleId="+roleId;
		useMaW_ch = initW_ch(useMaW_ch, "", true, 900, 520,'useMaW_ch');
		useMaW_ch.attachURL(url);
		useMaW_ch.setText("角色 "+pmGrid.cells(roleId,2).getValue()+":帐户管理");	
	    useMaW_ch.show();
	    useMaW_ch.bringToTop();	
	}
	var broAutW_ch;
	function broAuth(){
		var roleId = pmGrid.getSelectedId();
		if(roleId== null){
			hintMsg("请选择角色");
			return;
		}
		var url = conextPath+"/role/roleAction!browserAuth.action?dto.role.roleId="+roleId;
		broAutW_ch = initW_ch(broAutW_ch, "", true, 270, 500,'broAutW_ch');
		broAutW_ch.attachURL(url);
		broAutW_ch.setText("角色 "+pmGrid.cells(roleId,2).getValue()+":权限查看");	
	    broAutW_ch.show();
	    broAutW_ch.bringToTop();		
	}
	var grantW_ch;
	function authManager(){
		var roleId = pmGrid.getSelectedId();
		if(roleId== null){
			hintMsg("请选择角色");
			return;
		}
		var url = conextPath+"/role/roleAction!grantAuthInit.action?dto.role.roleId="+roleId;
		grantW_ch = initW_ch(grantW_ch, "", true, 270, 500,'grantW_ch');
		grantW_ch.attachURL(url);
		grantW_ch.setText("角色 "+pmGrid.cells(roleId,2).getValue()+":权限设置");	
	    grantW_ch.show();
	    grantW_ch.bringToTop();		
	}
	function deleteExe(){
		var roleName = pmGrid.cells(pmGrid.getSelectedId(),2).getValue();
		if(roleName=="超级管理员"){
			hintMsg("超级管理员不可删除");
			return;
		}
		var url = conextPath+"/role/roleAction!deleteRole.action?dto.role.roleId=";
		url += pmGrid.getSelectedId()+"&dto.isAjax=true";
		var ajaxRest = dhtmlxAjax.postSync(url, "findF").xmlDoc.responseText;
		if(ajaxRest=="success"){
			pmGrid.deleteRow(pmGrid.getSelectedId());
			clsoseCfWin();
			return;
		}
		hintMsg("删除时发生错误");
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var ajaxRest = dhtmlxAjax.postSync(url, "findF").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum();
   		}
   		pageSizec = 0;
	}
	var subUrl ="";
	function creater(id, divId){
		$("roleName").disabled=false;
		$("remark").disabled=false;
		$("adminObj").value="";
 		if(id=="update"){
			var roleName = pmGrid.cells(pmGrid.getSelectedId(),2).getValue();
			if(roleName=="超级管理员"&&isAdmin!="YES"){
				hintMsg("只有admin可以修改超级管理员角色");
				return;
			}
		}
		if(_isIE){
			cuW_ch = initW_ch(cuW_ch, divId, true, 420, 195);
		}else{
			cuW_ch = initW_ch(cuW_ch, divId, true, 420, 225);
		}
		formReset("createF", "roleName");
		$("roleId").value="";
		$("insertDate").value="";
		$("cUMTxt").innerHTML = "&nbsp;";
		if(id=="new"){		
			cuW_ch.setText("新建角色");
			subUrl=conextPath+"/role/roleAction!addRole.action?dto.isAjax=true";
			$("creat2_b").style.display="";
			$("accessIp").value="多个IP用分号(;)隔开";
		}else if(id=="update"){
			cuW_ch.setText("修改角色");
			$("creat2_b").style.display="none";
			var url = conextPath+"/role/roleAction!updRoleInit.action?dto.role.roleId=";
			url += pmGrid.getSelectedId()+"&dto.isAjax=true";
			var groupInfo  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			subUrl=conextPath+"/role/roleAction!updRole.action?dto.isAjax=true";
			if(groupInfo=="failed"){
				hintMsg("初始化数据发生错误");
			}else{
				setUpInfo(groupInfo);
			}
		}
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i <updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] !="null"){
				var valueStr ="$('"+currInfo[0]+"').value =currInfo[1]";
				eval(valueStr);	
				if(currInfo[0]=="roleName"&&currInfo[1]=="超级管理员"){
					$("roleName").disabled=true;
					$("remark").disabled=true;
					$("adminObj").value="YES";
					
				}
			}
		}
	}
	function ipChk(){
		var exp=/^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
		var ips = $("accessIp").value.split(";");
		if(ips.length==1){
			var reg = $("accessIp").value.match(exp);
			if(reg==null){
				$("cUMTxt").innerHTML = "非法的IP地址";
				return false;
			}else{
				return true;
			}		
		}
		for(var i=0; i<ips.length; i++){
		 	var ip = ips[i];
		 	if(ip.match(exp)==null){
		 		$("cUMTxt").innerHTML = "第" +(i+1) +"个IP为非法地址";
		 		return false;
		 	}
		}
		return true;
	} 
	function addUpSub(contFlg){
		if(isWhitespace($("roleName").value)){
			$("cUMTxt").innerHTML = "请填写角色名";
			return;
		}else if("多个IP用分号(;)隔开"==$("accessIp").value){
			$("accessIp").value="";
		}else if(!isWhitespace($("accessIp").value)&&!ipChk()){
			return;		
		}

		var ajaxRest = dhtmlxAjax.postSync(subUrl, "createF").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			$("cUMTxt").innerHTML = "保存数据时发生错误";
			return;
		}else if(ajaxRest.indexOf("success")==0){
			var rowData = "0,,"+$("roleName").value.replaceAll("，"," ")+",,"+$("remark").value;
			if($("roleId").value==""){
				pmGrid.addRow(ajaxRest.split("^")[1],rowData,0);
				pmGrid.setSizes();
			}else{
				rowData = "0,,"+$("roleName").value.replaceAll("，"," ")+","+pmGrid.cells($("roleId").value,3).getValue()+","+$("remark").value;
				pmGrid.deleteRow($("roleId").value);
				pmGrid.addRow($("roleId").value,rowData,0);
				pmGrid.setSizes();
			}
			setRowNum();
			if(typeof contFlg !="undefined"){
				$("cUMTxt").innerHTML = "操作成功请继续";
				formReset("createF", "roleName");
				return;
			}
			cuW_ch.setModal(false);
			cuW_ch.hide();
		}else if(ajaxRest=="reName"){
			$("cUMTxt").innerHTML = "角色重名";
		}
	}
	String.prototype.replaceAll  = function(s1,s2){   
    	return this.replace(new RegExp(s1,"g"),s2);
    }   
	function find(divId){
		fW_ch = initW_ch(fW_ch, divId, false, 460, 180);
		fW_ch.setText("查询");
		$("roleNameF").focus();
	}	
	function efind(){
		var ajaxRest = dhtmlxAjax.postSync(conextPath+"/role/roleAction!roleList.action?dto.isAjax=true&dto.pageSize=" + pageSize, "findF").xmlDoc.responseText;
	   	if(ajaxRest=="failed"){
	   		hintMsg("查询时发生错误");
	   		return;
	   	}
	   	var datas = ajaxRest.split("$");
	    if(datas[1] != ""){
	    	pmGrid.clearAll();
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setRowNum();
	    	setPageNoSizeCount(datas[0]);
	    	fW_ch.setModal(false);
	    	fW_ch.hide();
	    }else{
	    	hintMsg("没查到相关记录");
	    }
	}
	function initW_ch(obj, divId, mode, w, h,wId){
		importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w,h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.setDimension(w,h);
			obj.show();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != "")
				obj.attachObject(divId, false);	
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}	
	function initCufmsW(){//初始化window
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	var authTreeW_ch,htmlTreeWin
	function loadAuthTree(){
		if(typeof authTreeW_ch == "undefined"){
			authTreeW_ch = initW_ch(authTreeW_ch, "", true, 300, 470,'authTreeW_ch');
			var url = conextPath+"/role/roleAction!loadAuthTree.action";
			authTreeW_ch.attachURL(url);
			authTreeW_ch.setText("权限选择");	
		}
		authTreeW_ch.show();
		authTreeW_ch.bringToTop();
		authTreeW_ch.setModal(true);
		if(typeof(htmlTreeWin)!="undefined")
			htmlTreeWin.recorverChk();	
	}