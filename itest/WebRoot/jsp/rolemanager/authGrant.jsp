<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<title>权限设置</title>
	</head>
	<script type="text/javascript">
	popContextMenuFlg=0;
	</script>
	<body bgcolor="#ffffff" leftmargin="0" topmargin="3" marginwidth="0"  style="overflow-y:hidden;">
		<ww:hidden id="authTree" name="dto.authTree"></ww:hidden>
		<ww:hidden id="authIds" name="authIds"></ww:hidden>
		<ww:hidden id="roleId" name="dto.role.roleId"></ww:hidden>
		<ww:hidden id="operationId" name="operationId" value="${session.currentUser.userInfo.id}"></ww:hidden>
		<table width="100%">
		<tr>
		  <td>&nbsp;
		  </td>
		  <td>
		    <a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:parent.grantW_ch.setModal(false);parent.grantW_ch.hide();"
				style="margin-left: 6px;"><span> 返回</span> </a>
		   	<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="selChk()"
				style="margin-left: 6px;"><span> 确定</span> </a>
		  </td>
		  <td>&nbsp;
		  </td>
		  <td>&nbsp;
		  </td>
		  <td>
		  </td>
		</tr>
		</table>
		<div id="treeBox" style="overflow-y:hidden;width:100%;height:95%"></div> 
		<script>
		var initFunIds = "${dto.functionIds}";
		tree=new dhtmlXTreeObject("treeBox","100%","100%",0);					
		tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
		tree.enableCheckBoxes(true);
		tree.enableThreeStateCheckboxes(true);
		tree.enableHighlighting(1);
		tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
		tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
		tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	    tree.insertNewItem('0','-1','选择权限');
		var treeNodeArray = $("authTree").value.split(";");
		for(var i=0;i<treeNodeArray.length;i++){
			var nodeContext= treeNodeArray[i];
			var node = nodeContext.split(",");
			var parentId =node[0] ;
			var id  = node[1];
			var text = node[2];
			if(text.indexOf("#")>=0){ //带#的是占位菜单,暂时不用
				continue;
			}
			tree.insertNewItem(parentId,id,text);
		}
		if(initFunIds!=""){
			var roleAuth = initFunIds.split(" ");
			for(var j=0;j<roleAuth.length;j++ ){
				tree.setCheck(roleAuth[j],1);
			}		
		}
		function selChk(){
			$("authIds").value =  tree.getAllCheckedBranches().split(",");
			if($("authIds").value==""&&initFunIds==""){
				cfDialog("clsoseCfWin","请选择权限",true,200);
				return;
			}
			if($("authIds").value==""&&initFunIds!="")
				cfDialog('saveAuthInfo','确定要收回当前角色权限',false,200);
			else
			    cfDialog('saveAuthInfo','确定要授予当前角色所选权限?',false,200);
		}
		var selLeafNode = "";
		function saveAuthInfo(){
			var url = conextPath+"/role/roleAction!grantRoleAuth.action?dto.role.roleId="+parent.pmGrid.getSelectedId();
			var funIdsTemp= $("authIds").value.split(",");
			$("authIds").value = "";
			for(var i=0; i<funIdsTemp.length; i++){
				if(funIdsTemp[i]=="-1")
					continue;
				if(i<funIdsTemp.length-1)
					$("authIds").value +=funIdsTemp[i]+" ";
				else
				   $("authIds").value +=funIdsTemp[i];
				if(tree.hasChildren(funIdsTemp[i])<1)
					selLeafNode+=" " +funIdsTemp[i];
			}
			url+="&dto.functionIds="+$("authIds").value;
			if(!chkChg()){
				parent.grantW_ch.setModal(false);
				parent.grantW_ch.hide();
				clsoseCfWin();
				return;
			}
			var ajaxRest = dhtmlxAjax.postSync(url,"").xmlDoc.responseText;
			if(ajaxRest=="failed"){
				cfDialog("clsoseCfWin","保存权限发生错误",true,200);
				return;
			}
			clsoseCfWin();
			parent.grantW_ch.setModal(false);
			parent.grantW_ch.hide();
		}
		function chkChg(){
			if(initFunIds=="")
				return true;
			var funIdsTemp= selLeafNode.substring(1).split(" ");
			var funIdsOld = initFunIds.split(" ");
			if(funIdsTemp.length!=funIdsOld.length)
				return true;
			var eqFlag = true;
			for(var i=0; i<funIdsTemp.length;i++){
				if(!eqFlag)
					return false;
				for(var l=0; l<funIdsOld.length;l++){
					if(funIdsOld[l]==funIdsTemp[i]){
						eqFlag= false;
						break;
					}else{
						eqFlag= true;
					}
				}
			}
			return true;
		}
		</script>
		<ww:include value="/jsp/common/dialog2.jsp"></ww:include>
	</body>
</html>
