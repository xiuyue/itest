	pageBreakUrl ="";
	var uGrid;
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("选择,编号,Bug描述,状态,等级,项目名称,时机,类型,优先级,测试人员,开发人员,报告日期,msgFlag,realCase,moduleId,testOwnerId,currFlowCd,currHandlerId,currStateId,nextFlowCd,bugReptId,taskId");
    pmGrid.setInitWidths("40,80,"+nameWidth +",80,100,120,80,60,60,70,70,80,0,0,0,0,0,0,0,0,0,0");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
	pmGrid.enableRowsHover(true, "red");
	pmGrid.setMultiselect(true);
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,false,false,false,false,false,false,false,false,false,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    var chkAllBug=0;
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		if(!state&&chkAllBug==1){
			chkAllBug=0;
			pmBar.setItemImage("allChk", "ch_false.gif");
			pmBar.setItemToolTip("allChk", "全选");
		}else if(state&&chkAllBug==0){
		    var currChk= true;
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				if(pmGrid.cells(pmGrid.getRowId(i), 0).getValue()==0){
					currChk = false;
				}
			}
			if(currChk){				
				chkAllBug=1;
				pmBar.setItemImage("allChk", "ch_true.gif");
				pmBar.setItemToolTip("allChk", "取消全选");
			}			
		}		
		return true;
	}
    function doOnSelect(rowId,index){
		//pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
	}

	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	function getTttle(rowId,colIn){
		return pmGrid.cells(rowId,colIn).getValue();
	}
	loadLink();
	function loadLink(rowId){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			if(pmGrid.cells2(i,13).getValue()=="1"){
				pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:relaCase()' title='关联用例(蓝色表示己关联)'>"+getTttle2(i,1)+"</a>";
			}else{
				pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:relaCase()' title='关联用例'><font color='#339933'>"+getTttle2(i,1)+"</font></a>";
			}
			if(getTttle2(i,3).indexOf("<a href")<0){
				pmGrid.cells2(i,3).setValue("<a href='javascript:handBug()' title='处理问题'>"+getTttle2(i,3)+"</a>");
			}
			if(getTttle2(i,9).indexOf("<a href")<0){
				if(pmGrid.cells2(i,12).getValue()=="1"){
					pmGrid.cells2(i,9).cell.innerHTML="<a href='javascript:sendQuest()' title='意见交流(蓝色表示当前有给本人的消息)--"+getTttle2(i,9)+"'>"+getTttle2(i,9)+"</a>";
				}else{
					pmGrid.cells2(i,9).cell.innerHTML="<a href='javascript:sendQuest()' title='意见交流--"+getTttle2(i,9)+"'><font color='#339933'>"+getTttle2(i,9)+"</font></a>";
				}
			}
		}
		sw2Link();
	}
	function reSetLinkCol(colIndex,greenColr){
		colTypeReset();
		var bugId = pmGrid.getSelectedId();
		var rowNum = pmGrid.getRowIndex(bugId);
		var testName
		if(colIndex==9){
			testName = getTttle2(rowNum,colIndex).split(">")[1];
		}else{
			if(getTttle2(rowNum,colIndex).indexOf("#339933")<0&&typeof greenColr=="undefined"){
				sw2Link();
				return;
			}else if(getTttle2(rowNum,colIndex).indexOf("#339933")<0&&typeof greenColr!="undefined"){
				testName = getTttle2(rowNum,colIndex).split(">")[1];
				pmGrid.cells2(rowNum,colIndex).cell.innerHTML="<a href='javascript:relaCase()' title='关联用例'><font color='#339933'>"+testName+"</font></a>";
				sw2Link();
				return;
			}
			testName = getTttle2(rowNum,colIndex).split(">")[2];
		}
		testName = testName.substring(0,testName.indexOf("<"));
		if(colIndex==9){
			pmGrid.cells2(rowNum,colIndex).cell.innerHTML="<a href='javascript:sendQuest()' title='意见交流'><font color='#339933'>"+testName+"</font></a>";
		}else{
			pmGrid.cells2(rowNum,colIndex).cell.innerHTML="<a href='javascript:relaCase()' title='关联用例(蓝色表示己关联)'>"+testName+"</a>";
		}
		sw2Link();
	}
	function colTypeReset(){
		pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ch,link,ro,link,ro,ro,ro,ro,ro,link,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}	
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		opreType="query";
		pageAction(pageNo, pageSize);
		return;
	});
	function quickQuery(){
		var url =  conextPath+"/bugManager/bugManagerAction!quickQuery.action?dto.bug.bugId="+pmBar.getValue("qSearchIpt");
		var ajaxRest = postSub(url,"");
		if(ajaxRest==""){
			hintMsg("没查到相关记录");
			return;
		}
		pmGrid.clearAll();
		colTypeReset();
		var jsons = eval("(" + ajaxRest +")");
		pmGrid.parse(jsons, "json");
		cusSetPageNoSizeCount();
	   	loadLink();
	}
    function cusSetPageNoSizeCount(){
    	var tBar = pmBar;
		tBar.setItemText("pageP", pageSize);
		tBar.setValue("page", 1);
		tBar.setMaxValue("slider", 1, "");
		tBar.setItemText("pageMessage", "/ " + 1);
		tBar.setValue("slider", 1);
		tBar.disableItem("first");
		tBar.disableItem("pervious");
		tBar.disableItem("next");
		tBar.disableItem("last");
		tBar.disableItem("slider");		
	}
	pmBar.attachEvent("onEnter", function(id, value) {
		if(id=="qSearchIpt"){
			if(!isDigit(pmBar.getValue("qSearchIpt"), false)&&pmBar.getValue("qSearchIpt")!=""){
				pmBar.setValue("qSearchIpt", "");
				return;
			}
			quickQuery();
			return ;
		}
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		opreType="query";
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	var opreType="";
	pmBar.attachEvent("onClick", function(id) {
		opreType="query";
		if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}else if(id.length>30&&id!="00000000000000000000000000000000"){
			if($("customSprHomeTaskId").value==id)
				return;
			parent.parent.mypmMain.location=conextPath+"/bugManager/bugManagerAction!loadMyBug.action?dto.taskId="+id;
		}else if(id == "allChk"){
			chkAllReset();
		}
	});
	function chkAllReset(){
		if(chkAllBug==0){
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				pmGrid.cells(pmGrid.getRowId(i), 0).setValue(true);
			}		
			chkAllBug=1;
			pmBar.setItemImage("all", "ch_true.gif");
			pmBar.setItemToolTip("all", "取消全选");
		}else{
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				pmGrid.cells(pmGrid.getRowId(i), 0).setValue(false);
			}		
			chkAllBug=0;
			pmBar.setItemImage("all", "ch_false.gif");
			pmBar.setItemToolTip("all", "全选");
		}
	}
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			findInit();
		}else if(id == "assignPer"){
			if(getChecked()==""){
				hintMsg("请选择要分配的BUG");
				return;
			}
			importJs(conextPath+"/jsp/bugManager/assignPersion.js");
			popselWin();
		}
	});
	function getChecked(){
		selectItems = "";
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && pmGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
					$("assignProName").value=pmGrid.cells(items[i], 5).getValue();//写项目名称
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}
	function pageAction(pageNo, pSize){
		if(pageNo>pageCount&&opreType !="repeFind"){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var purl = pageUrl +"&dto.pageNo="+ pageNo +"&dto.pageSize=" + pSize;
		if(opreType =="repeFind"){
			purl = repeFinUrl +"&dto.pageNo="+ pageNo+"&dto.pageSize=" + pSize;
		}
		var ajaxRest = postSub(purl,"findForm");
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""&&opreType !="repeFind"){
			hintMsg("没查到相关记录");
			return;
		}else{
			$("listStr").value=ajaxRest;
			if(opreType =="repeFind"){
				handWin.popRepeWin();
				return;
			}
			colTypeReset();
			initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
	   		loadLink();
			sw2Link();
   		}
   		return;
	}
