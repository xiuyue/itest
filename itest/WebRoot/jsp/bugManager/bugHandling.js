	var detlW_ch,questW_ch,relCaseW_ch;
	//nextPageFlag 如不为空，就以它为准
	function handBug(moveFlag,nextPageFlag){
		var bugId = pmGrid.getSelectedId();
		if(typeof moveFlag!="undefined"&&typeof nextPageFlag=="undefined"){
			var rowNum = pmGrid.getRowIndex(bugId);
			var rowCount =pmGrid.getRowsNum();
			if("next"==moveFlag&&rowNum!=(rowCount-1)){
				rowNum = rowNum+1;
			}else if("next"==moveFlag&&rowNum==(rowCount-1)){
				if(pageNo==pageCount&&pageCount>1){
					pageAction(1, pageSize);
				}else if(pageNo<pageCount&&pageCount>1){
					pageAction(pageNo+1, pageSize);
				}
				rowNum=0;
			}else if("up"==moveFlag&&rowNum!="0"){
				rowNum = rowNum-1;
			}else if("up"==moveFlag&&rowNum=="0"){
				if(pageNo>1){
					pageAction(pageNo-1, pageSize);
					rowNum=pmGrid.getRowsNum()-1;
				}else if(pageNo==1&&pageCount>1){
					pageAction(pageCount, pageSize);
					rowNum=pmGrid.getRowsNum()-1;
				}else{
					rowNum = rowCount-1;
				}
			}
			bugId =pmGrid.getRowId(rowNum);
			pmGrid.setSelectedRow(bugId);
			pmGrid.cells(bugId,0).setValue(true);
		}else if(typeof nextPageFlag!="undefined"){
			pageAction(pageNo+1, pageSize);
			rowNum=0;
			bugId =pmGrid.getRowId(rowNum);
			pmGrid.setSelectedRow(bugId);
			pmGrid.cells(bugId,0).setValue(true);
		}
		try{collapseLayout('close');}catch(err){}
		detlW_ch = initW_ch(detlW_ch, "", true, 855, 550,'detlW_ch');
		var url=conextPath+"/bugManager/bugManagerAction!bugHand.action?dto.loadType=0&dto.bug.bugId="+bugId;
		var reportId = pmGrid.cells(bugId,20).getValue();
		var taskId = pmGrid.cells(bugId,21).getValue();
		if($("typeSelStr").value==""){
			url=conextPath+"/bugManager/bugManagerAction!bugHand.action?dto.loadType=1&dto.bug.bugId="+bugId;		
		}		
		url = url+"&dto.taskId="+taskId;
		
		detlW_ch.attachURL(url);
		detlW_ch.setText("");	
	    detlW_ch.show();
	    detlW_ch.bringToTop();
	    detlW_ch.setModal(true);
	    opreType="bugHand";
	}
	function getNextRowNum(bugId){
		var rowNum = pmGrid.getRowIndex(bugId);
		var rowCount =pmGrid.getRowsNum();
		if(rowNum!=(rowCount-1)){
			rowNum = rowNum+1;
		}else if(rowNum==(rowCount-1)){
			rowNum=0;
		}
		return 	rowNum;
	}
	function isLastRow(bugId){
		var rowNum = pmGrid.getRowIndex(bugId);
		var rowCount =pmGrid.getRowsNum();
		if(rowNum==(rowCount-1))
			return true;
		else
		    return false;
	}
	function isLastPage(){
		var currPageCount = pmBar.getValue("page");
		var allPageCount = pmBar.getItemText("pageMessage").substr(1);
		if(parseInt(currPageCount)<parseInt(allPageCount))
			return false;
		else
		   return true;
	}
	
	function selRowByNum(rowNum){
		bugId =pmGrid.getRowId(rowNum);
		pmGrid.setSelectedRow(bugId);
		pmGrid.cells(bugId,0).setValue(true);	
	}
	function sendQuest(){
		var bugId = pmGrid.getSelectedId();
		var url=conextPath+"/bugManager/bugShortMsgAction!loadMsgList.action?dto.shortMsg.bugId="+pmGrid.getSelectedId();
		var taskId = pmGrid.cells(bugId,21).getValue();
		url = url+"&dto.taskId="+taskId;
		questW_ch=initW_ch(questW_ch, "", true, 850, 535,'questW_ch');
		questW_ch.attachURL(url);
		questW_ch.setText("意见交流");	
	    questW_ch.show();
	    questW_ch.bringToTop();
	    questW_ch.setModal(true);
	}
	function relaCase(){
		var bugId = pmGrid.getSelectedId();
		var rowNum = pmGrid.getRowIndex(bugId);
		var url=conextPath+"/bugManager/relaCaseAction!loadRelaCase.action?dto.moduleId="+pmGrid.cells2(rowNum,14).getValue()+"&dto.bugId="+bugId;
		var taskId = pmGrid.cells(bugId,21).getValue();
		url = url+"&dto.taskId="+taskId;
		relCaseW_ch = initW_ch(relCaseW_ch, "", true, 850, 535,'relCaseW_ch');
		relCaseW_ch.attachURL(url);
		relCaseW_ch.setText("关联用例---点击序号查看明细---可跨页选择后关联");	
	    relCaseW_ch.show();
	    relCaseW_ch.bringToTop();
	    relCaseW_ch.setModal(true);
	}
