	var rootId = "";
	var tree=new dhtmlXTreeObject('treeBox',"100%","100%",0); 
	tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	tree.enableTreeLines(true);
	tree.enableHighlighting(1);
	tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	var mytreeArr = $("nodeDataStr").value.split(";");
	var topNode = "",newLoadNodeStr = "";
	for(var i=0 ;i<mytreeArr.length;i++){
		var nodeInfo = mytreeArr[i].split(",");
		tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
		if(parent.treeDisModel=="complex")
			newLoadNodeStr = newLoadNodeStr +nodeInfo[1] +"_"
		if(nodeInfo[3]=="0"&& i>0){
			tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
			tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
			tree.closeItem(nodeInfo[1]);
		}else if(i==0){
			rootId = nodeInfo[1];
		}
		setColorTip(nodeInfo[1],nodeInfo[2],nodeInfo[4]);
	}
	if(newLoadNodeStr!="")
		loadNodedetlData(newLoadNodeStr);
	tree.selectItem(rootId,false,false);
	$("currNodeId").value=rootId;
	tree.setOnClickHandler(onclickHdl);
	tree.setOnOpenEndHandler(onopenEndHdl);
	//importJs(conextPath + "/jsp/outlineManager/contextMenu.js");
	//tree.setOnRightClickHandler(showContextMenu);	
	function setColorTip(id ,text,nodeState){
		var hintText = "";
		if(nodeState==1){
			tree.setItemColor(id,'#ff0000','#ff0000');
			hintText="停用";
		}else if(nodeState==2){
			tree.setItemColor(id,'#66CD00','#66CD00');
			hintText="用例设计完成";
		}else if(nodeState==3){
			tree.setItemColor(id,'#668B8B','#668B8B');
			hintText="需求不明阻塞";
		}else if(nodeState==4){
			tree.setItemColor(id,'#698B22','#698B22');
			hintText="完成部分用例";
		}else if(nodeState==5){
			tree.setItemColor(id,'#8B8B7A','#8B8B7A');
			hintText="需求待定中";
		}
		tree.setItemText(id,text,hintText);
	}
	function onclickHdl(id){
		if(id==parent.mypmLayout.items[1]._frame.contentWindow.$("moduleIdF").value){		
			return;
		}
		parent.mypmLayout.items[1]._frame.contentWindow.$("moduleIdF").value = id;
		if(rootId!=id){
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleId").value = id;
			var moduleName = getMyCovPath(id);
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleName").value = moduleName;
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleNameF").value =moduleName;	
		}else{
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleId").value = "";
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleName").value ="";	
			parent.mypmLayout.items[1]._frame.contentWindow.$("moduleNameF").value ="所有";			
		}
		$("currNodeId").value= id;
		parent.mypmLayout.items[1]._frame.contentWindow.loadModuleBug(id);
		return;
	}

	function appendNode(treeArr,isOpening){
		var topNode = "";
		newLoadNodeStr="";
		for(var i=0 ;i<treeArr.length;i++){
			var nodeInfo = treeArr[i].split(",");
			topNode = nodeInfo[0];
			if(i==0){
				var blankId = tree.getUserData(nodeInfo[0],"blankCh");
				var haveBlank = typeof blankId=="undefined" ;
				if(!haveBlank){
					tree.deleteItem(blankId,false);
					tree.setUserData(topNode,"blankCh");
				}
			}
			if(parent.treeDisModel=="complex")
				newLoadNodeStr = newLoadNodeStr +nodeInfo[1] +"_"
			tree.insertNewItem(nodeInfo[0],nodeInfo[1],nodeInfo[2],0,0,0,0,"");
			tree.setUserData(nodeInfo[1],"MyState",nodeInfo[4]);
			setColorTip(nodeInfo[1],nodeInfo[2],nodeInfo[4]);
			if(nodeInfo[3]=="0"){
				tree.setUserData(nodeInfo[1],"blankCh",nodeInfo[1]+"null");
				tree.insertNewChild(nodeInfo[1],nodeInfo[1]+"null","",0,0,0,0,""); 
				tree.closeItem(nodeInfo[1]);
			}
			
		}
		if(newLoadNodeStr!="")
			loadNodedetlData(newLoadNodeStr);
		if(typeof isOpening!="undefined" ){
			tree.closeItem(topNode);
		}
		return;
	}
	function loadNodedetlData(nodeStrIds){
		var url = conextPath+"/caseManager/caseManagerAction!loadNodedetalData.action?dto.countStr="+nodeStrIds+"&dto.remark=onlyNormal";
		var ajaxResut = postSub(url,"");
		if(ajaxResut!=""&&ajaxResut!="failed"){
			var nodeDatas = ajaxResut.split(";");
			for(var i=0 ;i<nodeDatas.length;i++){
				var nodeData = nodeDatas[i].split(",");
				var text = tree.getItemText(nodeData[0])+"("+nodeData[1]+"/"+nodeData[2]+")";
				tree.setItemText(nodeData[0],text,tree.getItemTooltip(nodeData[0])+" 斜杠前后为用例数和BUG数  点击该测试需求项查看其下本人待处理BUG");
			}
		}	
	}
	function loadChildren(itemId,isOpening){
		var url = conextPath+"/caseManager/caseManagerAction!loadTree.action";
		url = url+"?dto.taskId="+$("taskId").value +"&dto.isAjax=true&dto.currNodeId="+itemId;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"&&ajaxResut!=""){
			var treeArr = ajaxResut.split(";");
			appendNode(treeArr,isOpening);
		}
		if(ajaxResut==""){
			var blankId = tree.getUserData(itemId,"blankCh");
			tree.deleteItem(blankId,true);
			tree.setUserData(itemId,"blankCh");
		}
		return;		
	}
	function onopenEndHdl(id,mode){
		if(mode<0){
			return ;
		}
		var blankId = tree.getUserData(id,"blankCh");
		var haveBlank = typeof blankId=="undefined" ;
		if(!haveBlank){
			loadChildren(id);
		}
		return;
	}
	function getAllParent(itemId,p){
		pId = tree.getParentId(itemId);
		if(pId==0){
			return p;
		}else{
			p+= ","+pId +getAllParent(pId,p)	
		}
		return p;
	}
	function getMyCovPath(id){
		var ids= id+getAllParent(id,"");
		var idArr=ids.split(",");
		var texts = "";
		for(var i=(idArr.length-1);i>=0;i--){
			texts= texts +"/"+tree.getItemText(idArr[i]).split("(")[0];
		}
		return texts.substring(1);
	}	