
	function upRow(rowId,rowData){
		pmGrid.deleteRow(rowId);
		colTypeReset();
		pmGrid._addRow(rowId,rowData,0);
		//setRowNum(pmGrid);
		loadLink(rowId);
		sw2Link();
		pmGrid.setSelectedRow(rowId);
		pmGrid.cells(rowId,0).setValue(true);		
	}
	function closeMain(){
		if(typeof popW_ch !="undefined"&&!popW_ch.isHidden()){
			popW_ch.setModal(false);
			popW_ch.hide();
		}
		cuW_ch.hide();
		cuW_ch.setModal(false);
	}
	function getMdPerson(loadType){
		if($("moduleId").value==""){
			hintMsg('当前没指定对应的测试需求');
			return;
		}
		var url = conextPath + "/bugManager/bugManagerAction!loadMdPerson.action?dto.currNodeId="+$("moduleId").value+"&dto.loadType="+loadType;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			if(loadType=="1")
				hintMsg('初始化测试需求对应开发人员失败');
			else 
			    hintMsg('初始化测试需求对应分配人员失败');
			return;
		}else if(ajaxRest==""){
			if(loadType=="1")
				hintMsg('当前测试需求没指定开发人员');
			else
			    hintMsg('当前测试需求没指定分配人员');
			return;		
		}
		$("moduleDevStr").value=ajaxRest;
		if(loadType=="1")
			popSelWin('devOwnerId','devOwnerName','moduleDevStr','修改人');
		else
		   popSelWin('assinOwnerId','anasnOwnerName','moduleDevStr','分配人');
	}
	//很奇怪，必须先初始化这窗口，要不修改页面中，FCK不能获得焦点
	var editW_ch=initW_ch(editW_ch, "", true, 830, 550,'editW_ch');
	editW_ch.hide();
	editW_ch.setModal(false);
	var initTaskId = "";
	function getInTaskRole(){
		var bugId = pmGrid.getSelectedId();
		var taskId = pmGrid.cells(bugId,21).getValue();
		if(initTaskId==taskId){
			return;
		}
		var url = conextPath + "/bugManager/bugManagerAction!getInTaskRole.action?dto.taskId="+taskId;
		var ajaxResut = postSub(url,"");
		roleStr = ajaxResut;
		initTaskId=taskId
	}

	function delExe(){
		var delId = pmGrid.getSelectedId();
		var reportId = pmGrid.cells(delId,20).getValue();
		 getInTaskRole();
		if(roleStr=="failed"){
			hintMsg("获取所选BUG所在项目的人员信息失败");
			return;
		}
		if(myId==reportId||roleStr.indexOf("8")>=0){
			var url = conextPath + "/bugManager/bugManagerAction!delete.action?dto.isAjax=true&dto.bug.bugId="+delId;
			var taskId = pmGrid.cells(delId,21).getValue();
			url = url+"&dto.taskId="+taskId;
			var ajaxResut = postSub(url,"");
			if(ajaxResut=="success"){
				pmGrid.deleteRow(delId);
				//setRowNum(pmGrid);
				mW_ch.setModal(false);
				mW_ch.hide();		
			}else{
				hintMsg("删除失败");
			}
			return;		
		}
		hintMsg("您不能执行删除");
	}
	function upInit(id){
		var reportId = pmGrid.cells(id,20).getValue();
		 getInTaskRole();
		if(roleStr=="failed"){
			hintMsg("获取所选BUG所在项目的人员信息失败");
			return;
		}
		if(myId==reportId||roleStr.indexOf("1")>=0||roleStr.indexOf("2")>=0||roleStr.indexOf("8")>=0){
			editW_ch = initW_ch(editW_ch, "", true, 830, 550,'editW_ch');
			var url= conextPath +"/bugManager/bugManagerAction!upInit.action?dto.isAjax=true&dto.loadType=0&dto.bug.bugId="+id;
			if($("typeSelStr").value==""){
				url=conextPath+"/bugManager/bugManagerAction!upInit.action?dto.isAjax=true&dto.loadType=1&dto.bug.bugId="+id;
			}
			var taskId = pmGrid.cells(id,21).getValue();
			url = url+"&dto.taskId="+taskId;
			editW_ch.attachURL(url);
			editW_ch.setText("修改软件问题报告");	
		    editW_ch.show();
		    editW_ch.bringToTop();
		    editW_ch.setModal(true);
		    opreType="update";
		    return;			
		}
		hintMsg('只有项目测试或BUG报告人员可以修改');
	}
	var treeW_ch,treeLoad;
	var editWin,handWin;
	var taskIdInit="";
	var noOutLine="1";
	function loadTree(win){
		if($("taskId").value==""){
			hintMsg('请先选择测试项目');
			return;
		}
		if(noOutLine=="1"){
			hintMsg('当前项目没提交测试需求,不能填写软件问题报告');
			return;
		}		
		if(typeof win != "undefined"){
			if(opreType=="bugHand"){
				handWin=win;
			}else{
				editWin = win;
			}
		}
		if(typeof treeLoad == "undefined"||(typeof treeLoad != "undefined"&&$("taskId").value!=taskIdInit)){
			treeW_ch = initW_ch(treeW_ch, "", true, 300, 470,'treeW_ch');
			var url = conextPath+"/bugManager/bugManagerAction!loadTree.action";
			treeW_ch.attachURL(url);
			treeW_ch.setText("测试需求分解树--------双击叶子节点选择");
			treeLoad="true";
			taskIdInit=$("taskId").value;
		}
		tyId="";
		treeW_ch.show();
		treeW_ch.bringToTop();
		treeW_ch.setModal(true);
		return;
	}

	function newQuery(chkObj){
		if(chkObj.checked){
			$("newQueryNameTr").style.display="";
			$("newQueryBtn").style.display="";
			$("upQuBtn").style.display="none";
		}else{
			$("newQueryNameTr").style.display="none";
			$("newQueryBtn").style.display="none";
			if($("currQueryId").value!=""){
				$("upQuBtn").style.display="";
			}
		}
		adjustTable("findTable");
	}
	function clearQueryTd(){
		$("queryNameF").value="";
		$("queryBtnTd").style.display="none";
		$("currQueryId").value="";
	}
	function addInitSetp2(){
		var url="";
		if($("typeSelStr").value==""){
			url=conextPath +"/bugManager/bugManagerAction!addInit.action?dto.isAjax=true&&dto.loadType=1&dto.taskId="+$("taskId").value;
		}else{
			url=conextPath +"/bugManager/bugManagerAction!addInit.action?dto.isAjax=true&&dto.loadType=0&dto.taskId="+$("taskId").value;
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg('初始化失败');
			noOutLine="1";
			return false;
		}else if("noOutLine"==ajaxResut){
			hintMsg("当前项目没提交测试需求,不能填写软件问题报告");
			var treeLoadTmp;
			treeLoad = treeLoadTmp;
			noOutLine="1";
			return false;		
		}
		noOutLine = "0";
		setUpInfo(ajaxResut);

	}
	var reSetFlg=1,oEditor ;
	function addInit(){
		if(reSetFlg==1){
			if(getCookie("clearFlg")==0){
				$("bugDesc").value="";			
			}else{
				$('createForm').reset();
				$("clearUpFlg").checked=false;
				disReset();
				resetHdeField();
				upVarReset();
			}
		}
		if(navigator.userAgent.indexOf("Chrome")>0)
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 500);
		else
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 485);
		cuW_ch.button("close").attachEvent("onClick", function(){
			actFlg= false;
			eraseAttach('eraseAllImg');
			cuW_ch.setModal(false);
			cuW_ch.hide();
		});
		cuW_ch.setText("新增软件问题报告");
		cuW_ch.setModal(true);
		cuW_ch.bringToTop();
		$("bugDesc").focus();
		loadFCK();
		if(getCookie("clearFlg")==0){
			try{oEditor.SetData("<strong>"+$("moduleName").value +"</strong>: ");}catch(err){}
		}
		opreType="add";
		adjustTable("createTable");
		actFlg= true;
		return true;	
	}
	var testProSelWin;
	function popTestProSelWin(){
		if(typeof(testProSelWin)!="undefined" &&testProSelWin.isHidden()){
	    	testProSelWin.show();
	    	testProSelWin.bringToTop();
	    	testProSelWin.setModal(true);
	    	return;			
		}
		testProSelWin = initW_ch(testProSelWin, "", true, 930, 500,'testProSelWin');
		var url = "/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=newBugSelTask";
		url = url +"&dto.taskIdField=taskId&dto.taskNameField=taskName";
		testProSelWin.attachURL(conextPath+url);
		testProSelWin.setText("请选择测试项目");	
    	testProSelWin.show();
    	testProSelWin.bringToTop();
    	testProSelWin.setModal(true);				
	}
	
	function findInit(win){
		if(typeof win=="undefined"){
			opreType="query";
		}else{
			handWin = win;
			opreType = "repeFind";
		}
		var url="";
		if($("typeSelStr").value==""){
			url=conextPath +"/bugManager/bugManagerAction!findInit.action?dto.isAjax=true&dto.loadType=1";
		}else{
			url=conextPath +"/bugManager/bugManagerAction!findInit.action?dto.isAjax=true&dto.loadType=0";
		}
		var ajaxResut = postSub(url,"");
		adjustTable("findTable");
		if(ajaxResut=="failed"){
			hintMsg('初始查询化失败');
			return ;
		}	
		fW_ch = initW_ch(fW_ch,  "findDiv", true,800, 320);
		fW_ch.button("close").attachEvent("onClick", function(){
			if(typeof(mypmCalendar_ch)!='undefined')
			 mypmCalendar_ch.hide();
		});
		$("bugDescF").focus();
		fW_ch.setText("查询");
		setUpInfo(ajaxResut);
		fW_ch.bringToTop();
	}
	function setUpInfo(updInfo){
		if(updInfo==""){
			return;
		}
		var updInfos = updInfo.split("^");
		var valueStr = "";
		var selStr = "";
		var selId = ""
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);	
				if(currInfo[0]=="ReProStep"){
					$("initReProStep").value=currInfo[1];
					initImg = getAttaInRichHtm(currInfo[1])
				}else if(currInfo[0]=="nextFlowCd"){
					flowControl_new(currInfo[1]);
				}else if(currInfo[0]=="testFlow"){
					flwStr=currInfo[1];
				}else if(currInfo[0]=="roleInTask"){
					roleStr =currInfo[1];
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					 $("currAttach").style.display="";
					 $("currAttach").title="附件";
					 //$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);			
				}			
			}
		}
	}
	function chgTestProReset(){
		$("testOwnerTr").style.display="none";
		$("analOwnerTr").style.display="none";
		$("anasnOwnerTr").style.display="none";
		$("devOwnerTr").style.display="none";
		$("intecsOwnerTr").style.display="none";
		$("assFromMdTd").style.display="none";	
		$("devOwnerId").value="";
		$("testOwnerId").value="";
		$("analyseOwnerId").value="";
		$("assinOwnerId").value="";
		$("intercessOwnerId").value="";	
	}
	function disReset(){
		$("impPhaTdtxt").style.display="none";
		$("impPhaTd").style.display="none";
		$("repropTd").style.display="none";
		$("reprop").style.display="none";
		$("geneCauseTd").style.display="none";
		$("geneCauseF").style.display="none";
		$("testOwnerTr").style.display="none";
		$("analOwnerTr").style.display="none";
		$("anasnOwnerTr").style.display="none";
		$("devOwnerTr").style.display="none";
		$("intecsOwnerTr").style.display="none";
		$("assFromMdTd").style.display="none";	
		$("anaAssFromMdTd").style.display="none";	
	}
	function flowControl_new(nextFlowCd){
		if(nextFlowCd=="2"){
			$("testOwnerTr").style.display="";
		}else if(nextFlowCd=="3"){
			$("analOwnerTr").style.display="";
		}else if(nextFlowCd=="4"){
			$("anasnOwnerTr").style.display="";
			$("anaAssFromMdTd").style.display="";	
		}else if(nextFlowCd=="5"){
			$("devOwnerTr").style.display="";
		}else if(nextFlowCd=="7"){
			$("intecsOwnerTr").style.display="";
		}
	}
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			if(reSetFlg==1)
				oEditor.SetData("");
			return;
		}
		importFckJs();
    	var pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('reProStep') ;
		if(reSetFlg==1)
			oEditor.SetData("");
		return;
	}	

	function addSubCheck(){
		if($("taskId").value==""){
			hintMsg("请选择对测试项目");
			return false;			
		}else if($("moduleId").value==""){
			hintMsg("请选择对应测试需求");
			return false;
		}else if($("bugReptVer").value==""){
			hintMsg("请选择发现版本");
			return false;		
		}else if(isAllNull($("bugDesc").value)){
			hintMsg("请填写问题描述");
			$("bugDesc").focus();
			return false;
		}else if($("bugTypeId").value==""){
			hintMsg("请选择类型");
			return false;
		}else if($("bugGradeId").value==""){
			hintMsg("请选择等级");
			return false;
		}else if($("platformId").value==""){
			hintMsg("请选择平台");
			return false;
		}else if($("sourceId").value==""){
			hintMsg("请选择来源");
			return false;
		}else if($("bugOccaId").value==""){
			hintMsg("请选择发现时机");
			return false;
		}else if($("geneCauseF").style.display==""&& $("geneCauseId").value==""){
			hintMsg("请选测试时机");
			return false;		
		}else if($("bugFreqId").value==""){
			hintMsg("请选择频率");
			return false;
		}else if($("reprop").style.display==""&&$("reproPersent").value==""){
			hintMsg("请填写再现比例");
			$("reprop").focus();
			return false;		
		}else if($("nextFlowCd").value=="2"&&$("testOwnerId").value==""){
			hintMsg("请选择互验人");
			return false;
		}else if($("nextFlowCd").value=="3"&&$("analyseOwnerId").value==""){
			hintMsg("请选择分析人");
			return false;
		}else if($("nextFlowCd").value=="4"&&$("assinOwnerId").value==""){
			hintMsg("请选择分配人");
			return false;
		}else if($("nextFlowCd").value=="5"&&$("devOwnerId").value==""){
			hintMsg("请选择修改人");
			return false;
		}else if($("taskId").value!=$("moduleIdTaskId").value){
			hintMsg("所选项目与测试需求不皮配");
			return false;
		}else if($("verSelStr").value.indexOf($("bugReptVer").value)<0){
			hintMsg("所选项目与发现发现版本不皮配");
			return false;
		}else if(!personChk()){
			return false;
		}else{
			$("reProStep").value=oEditor.GetXHTML().replace(/\s+$|^\s+/g,"");
			if($("reProStep").value=="<br />"||$("reProStep").value==""||$("reProStep").value=="<strong><br></strong>"){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			for(var i=1; i<31;i++){
				$("reProStep").value = $("reProStep").value.trim("<br>");
				$("reProStep").value = $("reProStep").value.trim("&nbsp;");	
				$("reProStep").value = $("reProStep").value.replace(/\s+$|^\s+/g,"");
				$("reProStep").value = $("reProStep").value.trim("\\");			
			}

			if(checkIsOverLong($("reProStep").value,1000)){
				hintMsg("再现步骤不能超过1000个字符");
				return false;			
			}
			var repTxt = $("reProStep").value.replace(/\s+$|^\s+/g,"");
			var mdPath = "<strong>"+$("moduleName").value +"</strong>:";
			if(repTxt==mdPath){
				hintMsg("请填写再现步骤");
				oEditor.Focus();
				return false;			
			}
			$("reProTxt").value=html2PlainText("reProStep");
		}
		if(checkIsOverLong($("bugDesc").value,150)){
			hintMsg("描述不能超过150个字符");
			return false;			
		}
		$("bugDesc").value = $("bugDesc").value.replace(/\s+$|^\s+/g,"");
		return true;
	}
	
	function personChk(){
		if($("nextFlowCd").value=="2"&&$("testSelStr").value.indexOf($("testOwnerId").value)<0){
			hintMsg("互验人不在当前测试项目中");
			return false;
		}else if($("nextFlowCd").value=="3"&&$("analySelStr").value.indexOf($("analyseOwnerId").value)<0){
			hintMsg("分析人不在当前测试项目中");
			return false;
		}else if($("nextFlowCd").value=="4"&&$("assignSelStr").value.indexOf($("assinOwnerId").value)<0){
			hintMsg("分配人不在当前测试项目中");
			return false;
		}else if($("nextFlowCd").value=="5"&&$("devStr").value.indexOf($("devOwnerId").value)<0){
			hintMsg("修改人不在当前测试项目中");
			return false;
		}
		return true;	
	}
	String.prototype.trim = function(){
	     var _argument = arguments[0]==undefined ? " ":arguments[0];
	     if(typeof(_argument)=="string"){
	        return this.replace(_argument == " "?/(^s*)|(s*$)/g : new RegExp("(^"+_argument+"*)|("+_argument+"*$)","g"),"");
	     }else if(typeof(_argument)=="object"){
	        return this.replace(_argument,"")
	     }else if(typeof(_argument)=="number" && arguments.length>=1){
	        return arguments.length==1? this.substring(arguments[0]) : this.substring(arguments[0],this.length-arguments[1]);
	     }
	};
	var contin ="";
	function resetHdeField(){
	    if(contin==""){
			$("taskId").value="";
		}
		$("moduleIdTaskId").value="";
		$("moduleId").value="";
		$("bugTypeId").value="";
		$("bugGradeId").value="";
		$("bugFreqId").value="";
		$("bugOccaId").value="";
		$("priId").value="";
		$("genePhaseId").value="";
		$("geneCauseId").value="";
		$("sourceId").value="";
		$("platformId").value="";
		$("devOwnerId").value="";
		$("testOwnerId").value="";
		$("analyseOwnerId").value="";
		$("assinOwnerId").value="";
		$("intercessOwnerId").value="";
		$("platformId").value="";
		$("cUMTxt").innerHTML = "";
		$("bugReptVer").value="";
		if(typeof oEditor != "undefined"){
			oEditor.SetData("");
		}
		
	}
	function newSub(){
		if(addSubCheck()){
			var url = conextPath + "/bugManager/bugManagerAction!add.action";
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					reSetFlg=1;
					eraseAttach();
					colTypeReset();
					pmGrid.addRow(result[1],result[2],0);
					if($("testOwnerTr").style.display==""){
						var testName = $("testOwnName").value;
						testName = testName.substring(testName.indexOf("(")+1,testName.length-1);
						pmGrid.cells(result[1],9).setValue($("testOwnName").value);
					}
					if($("devOwnerTr").style.display==""){
						var devOwnerName = $("devOwnerName").value;
						devOwnerName = devOwnerName.substring(devOwnerName.indexOf("(")+1,devOwnerName.length-1);
						pmGrid.cells(result[1],10).setValue($("devOwnerName").value);
					}
					//setRowNum(pmGrid);
					loadLink(result[1]);
					sw2Link();
					pmGrid.setSelectedRow(result[1]);	
					pmGrid.setSizes();	
					var currStateId=$("currStateId").value,stateName=$("stateName").value;
					var nextFlowCd=$("nextFlowCd").value;
					if(getCookie("clearFlg")==0){
						oEditor.SetData("<strong>"+$("moduleName").value +"</strong>: ");
						$("bugDesc").value="";	
					}else{
						resetHdeField();
						$("createForm").reset();
						$("clearUpFlg").checked=false;
						disReset();				
					}
					var taskNameTemp =$("taskName").value;
					$('upStatusBar').innerHTML="";
					
					if(contin!=""){
						$("cUMTxt").innerHTML =  "操作成功";
						$("bugDesc").focus();
						flowControl_new(nextFlowCd);
						$("currStateId").value=currStateId;
						$("stateName").value=stateName;
						$("nextFlowCd").value=nextFlowCd;
						upVarReset();
						$("taskName").value = taskNameTemp;
					}else{
						cuW_ch.setModal(false);
						cuW_ch.hide();
						actFlg= false;
					}
					contin="";
					return;					
				}else{
					$("cUMTxt").innerHTML =  "操作失败";
					contin="";
				}
			}else{
				$("cUMTxt").innerHTML =  "操作失败";
				contin="";
			}
		}
		contin="";
	}

	function delQuery(){
		if($("currQueryId").value==""){
			hintMsg('请选择要删的查询器');
			return;
		}
		cfDialog("delQueryExe","您确定删除选择的查询器?");
	}
	
	function delQueryExe(){
		var queryId = $("currQueryId").value;
		var url = conextPath+ "/bugManager/bugManagerAction!delQuery.action?dto.isAjax=true&dto.queryId="+queryId;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"){
			hintMsg('删除成功');
			$("querySelStr").value=delFromStr($("querySelStr").value,"$",queryId+";"+$("queryNameF").value);
			$("queryNameF").value="";
			$("currQueryId").value="";
			$("queryCount").value=parseInt($("queryCount").value)-1;
		}else{
			hintMsg('执行删除失败');
		}	
	}
	
	function delFromStr(StrVal,septor,intceptStr){
		var strTmp = StrVal.split(intceptStr);
		var tmpAf = strTmp[1];
		var tmpBe = strTmp[0];
		if(tmpBe!=""&&tmpBe.lastIndexOf("]")==tmpBe.length-1){
			tmpBe=tmpBe.substring(0,tmpBe.length-1);
			tmpBe=tmpBe.substring(0,tmpBe.indexOf("["));
		}
		if(tmpAf!=""&&tmpAf.indexOf("[")==0){
			tmpAf=tmpAf.substring(1,tmpAf.length);
			tmpAf=tmpAf.substring(tmpAf.indexOf("]")+1,tmpAf.length);
		}
		if(tmpBe!=""&&tmpBe.lastIndexOf(septor)==tmpBe.length-1){
				tmpBe=tmpBe.substring(0,tmpBe.length-1);
		}
		var subStr = tmpBe+tmpAf;
		if(subStr.indexOf(septor)==0){
			subStr=subStr.substring(1);
		}
		return subStr;
	}
	var pageUrl = conextPath+ "/bugManager/bugManagerAction!sw2AllMyBug.action?dto.allTestTask=true";
	var repeFinUrl = "";
	function findByquery(){
		if($("currQueryId").value==""){
			hintMsg('您没有选择查询器');
			return;
		}
		var url = conextPath+ "/bugManager/bugManagerAction!findByquery.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
		if(opreType !="repeFind"){
			url+="&dto.pageSize=" + pageSize;
		}else{
			var bugId = pmGrid.getSelectedId();
			url = url +"&dto.taskId="+pmGrid.cells(bugId,21).getValue();
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"){
			if(ajaxResut.split("$")[1]!=""){
				$("listStr").value=ajaxResut;
				if(opreType =="repeFind"){
					fW_ch.setModal(false);
					fW_ch.hide();
					handWin.popRepeWin();
					repeFinUrl= conextPath+ "/bugManager/bugManagerAction!findByquery.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
					if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
					return;
				}
				colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				sw2Link();
				fW_ch.setModal(false);
				fW_ch.hide();	
				if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
				pageUrl = conextPath+ "/bugManager/bugManagerAction!findByquery.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
			}else{
				hintMsg('没有查询到相关记录');
			}
		}else{
			hintMsg('执行查询时发生错误');
		}	
	}

	function query(saveFlg){
		var url = conextPath+ "/bugManager/bugManagerAction!sw2AllMyBug.action?dto.isAjax=true";
		if(opreType !="repeFind"){
			url+="&dto.pageSize=" + pageSize;
		}else{
			var bugId = pmGrid.getSelectedId();
			url = conextPath+ "/bugManager/bugManagerAction!loadMyBug.action?dto.taskId="+pmGrid.cells(bugId,21).getValue();
		}
		
		if(typeof saveFlg != "undefined"){
			url+="&dto.saveQuery=1";
			if(isAllNull($("queryName").value)){
				hintMsg('请填写查询器名');
				return;
			}
			if($("queryName").value.indexOf("]")>=0||$("queryName").value.indexOf("[")>=0){
				hintMsg('查询器名不能含[或]符号');
				return;		
			}
			if(!saveQueryChk()){
				return;
			}
		}
		if(opreType !="repeFind"){
			url+="&dto.allTestTask=true";
		}
		
		var ajaxResut = postSub(url,"findForm");
		if(ajaxResut!="failed"){
			var data = ajaxResut.split("$");
			if(typeof saveFlg != "undefined"){
				if($("queryCount").value>0&&$("querySelStr").value!=""){
					$("querySelStr").value+="$"+data[0]+";"+getQeyAliasName();
				}
				$("queryCount").value ="1" ;
				$("listStr").value = data[1]+"$"+data[2];
			}else{
				$("listStr").value=ajaxResut;
			}
			if($("listStr").value.split("$")[1]!=""){
				if(opreType =="repeFind"){
					fW_ch.setModal(false);
					fW_ch.hide();
					handWin.popRepeWin();
					repeFinUrl=conextPath+ "/bugManager/bugManagerAction!loadMyBug.action?dto.isAjax=true";
					if(typeof(mypmCalendar_ch)!="undefined")
						mypmCalendar_ch.hide();
					return;
				}
			 	colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				sw2Link();
				fW_ch.setModal(false);
				fW_ch.hide();
				if(typeof(mypmCalendar_ch)!="undefined")
					mypmCalendar_ch.hide();
				pageUrl = conextPath+ "/bugManager/bugManagerAction!sw2AllMyBug.action?dto.allTestTask=true";	
			}else{
				hintMsg('没有查询到相关记录');
			}
		}else{
			hintMsg('执行查询时发生错误');
		}	
	}
	function getQeyAliasName(){
		var quryAliasNme = $("queryName").value;
		if($("defBugId").checked){
			if($("appScope").checked){
				quryAliasNme+="[与我有关--跨任务]";
			}else{
				quryAliasNme+="[与我有关]";
			}
		}else if($("appScope").checked){
			quryAliasNme+="[跨任务]";
		}
		return 	quryAliasNme;
	}
	function queryDtal(){
		if($("currQueryId").value==""){
			hintMsg('您没选择查询器');
			return;
		}
		var url = conextPath+ "/bugManager/bugManagerAction!queryDetail.action?dto.isAjax=true&dto.queryId="+$("currQueryId").value;
		var ajaxResut = postSub(url,"");
		if(ajaxResut!="failed"){
			resetQuery("show");
			setQuryDtalInfo(ajaxResut);
			$("upQuBtn").style.display="";
			$("saveQueryN").checked=false;
			$("newQueryNameTr").style.display="";
			return;
		}else if(ajaxResut.indexOf("failed^")!=-1){
			hintMsg('查询器己被删除');
		}
		hintMsg('加载查询器发生错误');
	}
	
	function setQuryDtalInfo(queryInfo){
		if(queryInfo==""){
			return;
		}
		var queryInfos = queryInfo.split("^");
		for(var i=0; i < queryInfos.length; i++){
			var currInfo = queryInfos[i].split("=");
			if(currInfo[1] != "null"&&currInfo[0]!="taskId"&&currInfo[0]!="defBugId"&&currInfo[0]!="appScope"){
				if(currInfo[0]=="bugDescF"){
					currInfo[1] = currInfo[1].substring(1,currInfo[1].length-1);
				}
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);	
				loadName(currInfo[0],currInfo[1]);
			}
			if(currInfo[0]=="appScope"&&currInfo[1]=="1"){
				$("appScope").checked=true;
			}else if(currInfo[0]=="defBugId"&&currInfo[1]=="1"){
				$("defBugId").checked=true;
			}else if(currInfo[0]=="defBugId"&&currInfo[1]=="0"){
				$("defBugId").checked=false;
			}
			if(currInfo[0]=="moduleIdF"&&currInfo[1]!=""){
				$("moduleIdFInit").value=currInfo[1];
			}
			if(currInfo[0]=="bugReptVerF"&&currInfo[1]!=""){
				$("bugReptVerFInit").value=currInfo[1];
			}
		}	
	}
	function loadName(fieldId,fieldVal){
		if(fieldId=="genePhaseIdF"){ 
 			$("genPhNameF").value=getName($("genePhaseSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugFreqIdF"){
		    $("bugFreqNameF").value=getName($("freqSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="priIdF"){
		     $("priNameF").value=getName($("priSelStr").value,fieldVal,fieldId);   
		}else if(fieldId=="geneCauseIdF"){
		  	$("geneCaseNameF").value=getName($("geCaseSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugOccaIdF"){
		    $("occaNameF").value=getName($("occaSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="sourceIdF"){
		    $("sourceNameF").value=getName($("sourceSelStr").value,fieldVal,fieldId); 
		}else if(fieldId=="platformIdF"){ 
		 	$("pltfomNameF").value=getName($("plantFormSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugGradeIdF"){ 
		 	$("bugGradeNameF").value=getName($("gradeSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugTypeIdF"){
			$("bugTypeNameF").value=getName($("typeSelStr").value,fieldVal,fieldId);
		}else if(fieldId=="bugReptVerF"){
			$("bugReptVerNameF").value=getName($("verSelStr").value,fieldVal,fieldId);
		}		
	}
	function getName(selStr,fieldVal,fieldId){
		if(selStr.indexOf(fieldVal+";")<0){
			$(fieldId).value="-1";
			return;
		}
		var nameTmp = selStr.split(fieldVal+";");
		if(nameTmp[1].indexOf("$")>0){
			return nameTmp[1].substring(0,nameTmp[1].indexOf("$"));
		}else{
			return nameTmp[1];
		}	
	}
	function saveQuery(upFlat){
		if(isAllNull($("queryName").value)){
			hintMsg('请填写查询器名');
			return;
		}
		if($("queryName").value.indexOf("]")>=0||$("queryName").value.indexOf("[")>=0){
			hintMsg('查询器名不能含[或]符号');
			return;		
		}
		if(!saveQueryChk()){
			return;
		}
		$("queryId").value="";
		if(typeof upFlat != "undefined"){
			$("queryId").value=$("currQueryId").value;
		}
		var firstLoad=0;
		var url = conextPath+ "/bugManager/bugManagerAction!saveQuery.action";
		if($("queryCount").value>0&&$("querySelStr").value==""){
			url += "&dto.loadType=1";
			firstLoad=1;
		}
		reCoverNoUseCon();
		var ajaxResut = postSub(url,"findForm");
		if(firstLoad==1){
			if(ajaxResut!="failed"){
				$("querySelStr").value=ajaxResut;
				hintMsg('新增查询器成功');
				$("upQuBtn").style.display="";
				$("newQueryBtn").style.display="none";
				$("saveQueryN").checked=false;
				$("queryCount").value="1";
				reSetNoUseCon();
				return;
			}else{
				hintMsg("初始化查询器失败");
				reSetNoUseCon();
				return;
			}		
		}
		
		if(ajaxResut.indexOf("sucess^")==0){
			var qId = ajaxResut.split("^")[1];
			if($("queryCount").value>0){
				$("querySelStr").value+="$"+qId+";"+getQeyAliasName();
			}else{
				$("querySelStr").value+=qId+";"+getQeyAliasName();
			}
			$("upQuBtn").style.display="";
			$("newQueryBtn").style.display="none";
			$("saveQueryN").checked=false;
			hintMsg('新增查询器成功');
			reSetNoUseCon();
		}else if(ajaxResut=="sucess"){
			var queryArr = $("querySelStr").value.split("$");
			for(var i=0; i<queryArr.length; i++){
				if(queryArr[i].split(";")[0]==$("queryId").value){
					$("querySelStr").value = $("querySelStr").value.replace(queryArr[i],$("queryId").value+";"+getQeyAliasName());
					$("queryNameF").value = $("queryName").value;
					$("saveQueryN").checked=false;
					break;
				}
			}
			$("queryId").value="";
			hintMsg('更新查询器成功');
			reSetNoUseCon();
			$("upQuBtn").style.display="";
			$("newQueryBtn").style.display="none";
			$("saveQueryN").checked=false;
		}else{
			hintMsg('操作失败');
			reSetNoUseCon();
		}
	}
	function saveQueryChk(){
		var form = $("findForm");
		var elements = form.elements;
		var praCount = 0;
		for (i = 0; i < elements.length; i++){
			if(elements[i].type == "select"&&elements[i].value!="-1"&&elements[i].value!=""){
				praCount++;
			}else if(elements[i].type == "hidden"&&elements[i].value!="-1"&&elements[i].value!=""){
				praCount++;
			}
		}
		if(praCount>=3){
			return true;
		}
		hintMsg('查询条件不能少于3个');
		return false;
	}
	function resetQuery(mdBtnHide){
		$("newQueryNameTr").style.display="none";
		$("newQueryBtn").style.display="none";
		var queryNameF = $("queryNameF").value;
		findForm.reset();
		$("queryNameF").value= queryNameF;
		$("upQuBtn").style.display="none";
		$("genePhaseIdF").value="-1";
		$("bugFreqIdF").value="-1";
		$("priIdF").value="-1";
		$("geneCauseIdF").value="-1";
		$("bugOccaIdF").value="-1";
		$("sourceIdF").value="-1";
		$("platformIdF").value="-1";
		$("bugGradeIdF").value="-1";
		$("bugTypeIdF").value="-1";
		$("devOwnerIdF").value="-1";
		$("testOwnerIdF").value="-1";
		$("moduleIdF").value="-1";
		$("queryId").value="-1";
		$("nextOwnerIdF").value="-1";		
	}
	function loadTaskList(){
		var rest =dhtmlxAjax. postSync(conextPath+"/commonAction!loadTaskSwList.action","").xmlDoc.responseText;
		if(rest=="failed"){
			hintMsg("加载测试任务切换列表失败");
			return;
		}else if(rest==""&&$("customallMySprHome").value==""){
			hintMsg("您没参与任何测试任务");
			return;		
		}else if(rest==""){
			return;		
		}
		var optsArr = rest.split("&");
		for(var i=0;i<optsArr.length;i++){
			var currOpt = optsArr[i].split("^");
			pmBar.addListOption('taskList', currOpt[0], (i+1), "button",  currOpt[1]);
		}
		if($("customallMySprHome").value!=""&&rest.indexOf($("customallMySprHome").value)>=0){
			pmBar.setListOptionSelected('taskList', $("customallMySprHome").value);
			pmBar.setItemText("taskList", pmBar.getListOptionText("taskList", $("customallMySprHome").value));
		}
	}
	function setCookie(obj, time){
		var date=new Date();
		date.setTime(date.getTime()+ 2592000000);
		document.cookie = obj + "; expires=" + date.toGMTString();
	}
		
	function getCookie(key){
		var strCookie = document.cookie;
		var arrCookie = strCookie.split("; ");
		var value = "";
		for(var i = 0; i < arrCookie.length; i++){
			var arr = arrCookie[i].split("=");
			if(key == arr[0]){
				value = arr[1];		
				break;
			}
		}
		return value;
	}
	function setClearFlg(chkObj){
		if(!chkObj.checked){
			setCookie("clearFlg=1", 2592000000);			
		}else{
			setCookie("clearFlg=0", 2592000000);
		}
	}