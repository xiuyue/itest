	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h,wId){
		loadCSS(conextPath+"/dhtmlx/windows/codebase/dhtmlxwindows.css");
		loadCSS(conextPath+"/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css");
		importJs(conextPath + "/dhtmlx/windows/codebase/dhtmlxwindows.js");
		if((typeof obj != "undefined")&& !obj.isHidden()){
			obj.setDimension(w, h);
			obj.centerOnScreen();
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
			obj.centerOnScreen();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			obj.centerOnScreen();
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}
	var popW_ch;
	var popGrid;
	var tyId,tpNa;
	var sTime =0;
	function popSelWin(passValId,passNameId,dataId,title,taskScope){
		
		if(typeof (taskScope)=="undefined"&&passValId!="currQueryId"){
		//如是在我的BUG处报BUG，先要选择测试项目
			if($("taskId")!=null&&typeof $("taskId").value !="undefined"&&$("taskId").value==""){
				hintMsg("请先选择测试项目");
				return;
			}		
		}
		if(passValId==tyId&&((new Date()).getTime()-sTime)<1000){
			if(typeof(cuW_ch) !="undefined")
				cuW_ch.setModal(false);
			fW_ch.setModal(false);
			popW_ch.show();
			popW_ch.bringToTop();
			popW_ch.setModal(true);
			return;
		}
		
		sTime = (new Date()).getTime();
		tyId = passValId;
		tpNa = passNameId;
		if(typeof treeW_ch != "undefined"&&treeW_ch.isModal()){
			treeW_ch.setModal(false);
		}
		if(tyId=="currQueryId"&&$("queryCount").value>0&&$("querySelStr").value==""){
			var url = conextPath+ "/bugManager/bugManagerAction!loadQueryList.action?dto.isAjax=true"; 
			if(typeof (taskScope)=="undefined"&&$("customSprHomeTaskId")!=null){
				url = url +"&dto.taskId="+$("customSprHomeTaskId").value;
			}
			
			var ajaxResut = postSub(url,"");
			if(ajaxResut!="failed"){
				$("querySelStr").value=ajaxResut;
			}else{
				hintMsg("初始化查询器失败");
				return;
			}
		}
		//如果是设置版本数据，在所有项目模式下，要判断要不要重加载版本数据
		if(passValId=="fixVer"||passValId=="verifyVer"||passValId=="bugReptVer"){
			if((bugTaskId!= $("taskId").value&&bugTaskId!="0")||$(dataId).value==""){
				var url = conextPath + "/testTaskManager/testTaskManagerAction!loadVerSel.action";
				var ajaxRest = postSub(url,"");	 
				if(ajaxRest=="failed"){
					hintMsg('加载版本信息失败');
					return;
				}else{
					$(dataId).value = replaceAll(ajaxRest,"^","$");
				}				
			}
		}
		if(typeof popW_ch == "undefined"){
			popW_ch = initW_ch(popW_ch, "", true, 300, 230,'popW_ch');
			popGrid = popW_ch.attachGrid()
			popGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			popGrid.setHeader(",,单击选择");
			popGrid.setInitWidths("0,0,*");
	    	popGrid.setColAlign("left,left,left");
	    	popGrid.setColTypes("ch,ro,ro");
	   		popGrid.setColSorting("Str,int,str");
	   		popGrid.enableTooltips("false,false,true");
	    	popGrid.enableAutoHeight(true, 180);
	    	popGrid.enableRowsHover(true, "red");
	        popGrid.init();
	    	popGrid.setSkin("light");
		    popGrid.attachEvent("onRowSelect",function (rowId,index){
		    	if(rowId!="-1"){
		    		$(tyId).value=rowId;
		    		if(tyId=="testOwnerId"||tyId=="analyseOwnerId"||tyId=="assinOwnerId"||tyId=="devOwnerId"){
		    			$("nextOwnerId").value=rowId;
		    		}
		    		var vName = popGrid.cells(rowId,2).getValue();
		    		if(tyId=="currQueryId"&&vName.lastIndexOf("[")>0&&vName.lastIndexOf("]")>0){
		    			$(tpNa).value = vName.substring(0,vName.lastIndexOf("["));
		    		}else{
		    			$(tpNa).value = vName;
		    		}
		    	}else{
		    		$(tpNa).value="";
		    		$(tyId).value=""
		    	}
	   			if("bugFreqId"==tyId){
	   				if($(tpNa).value=="偶尔出现"){
	   					$("repropTd").style.display="";
						$("reprop").style.display="";	   				
	   				}else{
	   					$("repropTd").style.display="none";
						$("reprop").style.display="none";
					}
	   			}else if("bugOccaId"==tyId){
	   				if( $(tpNa).value=="首次出现"){
	   					$("geneCauseTd").style.display="";
						$("geneCauseF").style.display="";	   				
	   				}else{
	   					$("geneCauseTd").style.display="none";
						$("geneCauseF").style.display="none";	   				
	   				}
	   			}else if(tyId=="currQueryId"&&rowId!="-1"){
	   				$("queryBtnTd").style.display="";
	   				$("newQueryBtn").style.display="none";
	   				$("saveQueryN").checked=false;
	   				$("upQuBtn").style.display="none";
	   			}
				popW_ch.hide();
				popW_ch.setModal(false);
			});
		}
		popW_ch.setText(title);
		popW_ch.center();
		popGrid.clearAll();
		var selData = $(dataId).value;
		if(selData!=""){
			selDatas = selData.split("$");
			for(var i = 0; i < selDatas.length; i++){
				var items = selDatas[i].split(";")
				popGrid.addRow(items[0],"0,,"+items[1],i);
			}
			if((opreType=="query"||opreType=="repeFind")&&tyId!="currQueryId"){
				popGrid.addRow("-1","0,,----所有------",0);
			}
			if($(tyId).value!=""&&popGrid.getRowIndex($(tyId).value)>=0){
				popGrid.setRowColor($(tyId).value, "#CCCCCC");
			}else if(opreType=="query"&&tyId!="currQueryId"){
				popGrid.setRowColor("-1", "#CCCCCC");
			}
		}
		if(tyId=="currQueryId"&&selData==""){
			popGrid.addRow('-1',"0,,您此前没有保存任何查询为查询器!请填写查询条件查询");
		}
		if(typeof(cuW_ch) !="undefined")
			cuW_ch.setModal(false);
		if(typeof(fW_ch) !="undefined")
			fW_ch.setModal(false);
		popW_ch.show();
		popW_ch.bringToTop();
		popW_ch.setModal(true);
		bugTaskId= $("taskId").value;
		return;
	}
