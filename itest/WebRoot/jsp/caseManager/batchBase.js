	function loadCase(id){
		$("currNodeIdF").value=id
		var url=conextPath+"/caseManager/caseManagerAction!batchAuditInit.action?dto.isAjax=true";
		var ajaxResut;
		//if($("remQuery").checked==false){
			//ajaxResut =  postSub(url,"");
			//pfromReset();
		//}else{
			ajaxResut = postSub(url,"findForm");
			//cp2Pform();
		//}
		$("listStr").value=ajaxResut;
		if(typeof dW_ch != "undefined" && !dW_ch.isHidden()){
			dW_ch.hide();
			dW_ch.setModal(false);
		}
		if(typeof cW_ch != "undefined" && !cW_ch.isHidden()){
			cW_ch.hide();
			cW_ch.setModal(false);
		}
		if(typeof mW_ch != "undefined" && !mW_ch.isHidden()){
			mW_ch.hide();
			mW_ch.setModal(false);
		}
		if(typeof ptW_ch != "undefined" && !ptW_ch.isHidden()){
			ptW_ch.hide();
			ptW_ch.setModal(false);
		}	
		if(typeof cuW_ch != "undefined" && !cuW_ch.isHidden()){
			cuW_ch.hide();
			cuW_ch.setModal(false);
		}				
	    if(ajaxResut!="failed"){
	    	pmGrid.clearAll();
	    	 colTypeReset();
			initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
			loadLink();
	    }
	    return;			
	}
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	function quickQuery(){
		var url =  conextPath+"/caseManager/caseManagerAction!quickQuery.action?dto.testCaseInfo.testCaseId="+pmBar.getValue("qSearchIpt");
		var ajaxRest = postSub(url+"&dto.testCaseInfo.testStatus=0","");
		if(ajaxRest==""){
			hintMsg("没查到相关记录");
			return;
		}
		pmGrid.clearAll();
		colTypeReset();
		var jsons = eval("(" + ajaxRest +")");
		pmGrid.parse(jsons, "json");
		cusSetPageNoSizeCount();
	   	loadLink();
	}
    function cusSetPageNoSizeCount(){
    	var tBar = pmBar;
		tBar.setItemText("pageP", pageSize);
		tBar.setValue("page", 1);
		tBar.setMaxValue("slider", 1, "");
		tBar.setItemText("pageMessage", "/ " + 1);
		tBar.setValue("slider", 1);
		tBar.disableItem("first");
		tBar.disableItem("pervious");
		tBar.disableItem("next");
		tBar.disableItem("last");
		tBar.disableItem("slider");		
	}
	pmBar.attachEvent("onEnter", function(id, value) {
		if(id=="qSearchIpt"){
			if(!isDigit(pmBar.getValue("qSearchIpt"), false)&&pmBar.getValue("qSearchIpt")!=""){
				pmBar.setValue("qSearchIpt", "");
				return;
			}
			quickQuery();
			return ;
		}
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	var chkIds = "";
	pmBar.attachEvent("onClick", function(id) {
		if(id == "ldTree"){
			loadTree();
		}else if(id == "adtInit"){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请选择用例");
				return;
			}
			adtInit();
		}else if(id == "viewh"){
			viewHisty();
		}else if(id == "find"){ 
			findInit();
		}else if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}else if(id=="sw2Task"){
			swTestTask();
		}else if(id=="expCase"){
			$('currNodeIdF').value=$('currNodeId').value;
			$('findForm').submit();
		}else if(id=="batchAudit"){
			window.location=conextPath+"/caseManager/caseManagerAction!batchAuditInit.action?dto.taskId="+id;
		}else if(id == "allChk"){
			chkAllReset();
		}else if(id == "adtPass"){
			chkIds = getChecked();
			if(chkIds==""){
				hintMsg("请选择要审批的用例");
				return;			
			}		
			cfDialog('auditPass','当前操作不可逆，确定要提交测试?');
		}else if(id == "adtFailed"){
			chkIds = getChecked();
			if(chkIds==""){
				hintMsg("请选择要审批的用例");
				return;
			}
			cfDialog('auditFailed','当前操作不可逆，确定要打回修正?');
		}
	});
	function auditFailed(){
		auditExe('6');
	}	
	function auditPass(){
		auditExe('1');
	}
	function auditExe(adtRest){
		var url = conextPath+"/caseManager/caseManagerAction!batchAuditSub.action?dto.command="+adtRest+"&dto.pageSize="+pageSize;
		$("currNodeIdF").value = $("currNodeId").value;
		$("remarkF").value = chkIds;
		var ajaxRest = postSub(url, "findForm");
		if(ajaxRest=="failed"){
			hintMsg("写审批结果发生错误");	
		}else{
			$("listStr").value=ajaxRest;
			pmGrid.clearAll();
			colTypeReset();
			initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
			hintMsgClose();
			loadLink();
			cp2Pform();
			return;		
		}			
		chkIds = "";
	}	
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var purl = pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		purl+="&dto.currNodeId="+$("currNodeId").value;
		var ajaxRest = postSub(purl,"findForm");
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			pmGrid.clearAll();
			colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		//setRowNum(pmGrid);
	   		loadLink();
   		}
   		return;
	}

	function cp2Pform(){
		$("priIdP").value=$("priIdF").value;
		$("caseTypeIdP").value=$("caseTypeIdF").value;
		$("testRestP").value=$("testRestF").value;
		$("createrIdP").value=$("createrIdF").value;
		$("testAuditIdP").value=$("testAuditIdF").value;
		$("weightP").value=$("weightF").value;
		$("testCaseDesP").value=$("testCaseDesF").value;
	}
	function pfromReset(){
		$("priIdP").value=-1;
		$("caseTypeIdP").value=-1;
		$("testRestP").value=-1;
		$("createrIdP").value="";
		$("testAuditIdP").value="";
		$("weightP").value="";
		$("testCaseDesP").value="";	
	}
	function viewHisty(){
		if($("currNodeId").value==""){
			loadTree();
			hintMsg("请从需求分解树中选功能测试需求");
			return;
		}
		cuW_ch = initW_ch(cuW_ch, "", true, 960, 520,'cuW_ch')
		cuW_ch.setText("执行记录---点击序号关联BUG---点击用例描例查看明细");
		var url = conextPath+"/caseManager/caseManagerAction!viewCaseHistory.action?dto.testCaseInfo.testCaseId="+$("currNodeId").value;
		cuW_ch.attachURL(url);
	}
	secuW_ch = initW_ch(secuW_ch,  "", true,700, 430);
	secuW_ch.hide();
	secuW_ch.setModal(false);
	var currCmd = "";	
	function viewDetalSw(detal){
		$("testCaseDes").readOnly=detal;
		$("expResult").readOnly=detal;
		$("weight").readOnly=detal;
		$("priId").disabled =detal;
		$("caseTypeId").disabled=detal;
		if(detal){
			$("saveBtn").style.display="none";
			$("insertImg").style.display="none";
		}else{
			$("saveBtn").style.display="";
			$("insertImg").style.display="";
			$("currUpFile").style.display="";
		}
	}
	function setUpInfo(updInfo){
		if(updInfo=="")
			return;
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if(currInfo[0]=="typeSelStr"||currInfo[0]=="priSelStr"){
					var selStr = currInfo[1];
					var selId="priId";
					if(currInfo[0]=="typeSelStr"){
						selId="caseTypeId";
					}
					loadSel(selId,selStr);	
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					eval(valueStr);	
					if(currInfo[0]=="expResult"){
						$("expResultOld").value=currInfo[1];
					}else if(currInfo[0]=="operDataRichText"){
						$("initOpd").value=currInfo[1];
						initImg = getAttaInRichHtm(currInfo[1]);
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
						$("currAttach").style.display="";
						$("currAttach").title="附件";
						//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]==""){
						$("currAttach").style.display="none";
					}else if(currInfo[0]=="remark")
						$("adtRemark").value= currInfo[1];
				}
			}
		}
	}
	function loadSel(selId,selStr,splStr){
		if(selStr==""){
			return;
		}
		$(selId).options.length = 1;
		var options ;
		if(splStr){
			options = selStr.split(splStr);
		}else{
			options = selStr.split("$");
		}
		for(var i = 0; i < options.length; i++){
			var optionArr = options[i].split(";");
			if(optionArr[0] != "")
				var selvalue = optionArr[0] ;
				var selable = optionArr[1];
				$(selId).options.add(new Option(selable,selvalue));
		}	
	}
	var oEditor ;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			if($("testCaseId").value == ""){
				oEditor.SetData("<strong>" +$("mdPath").value+"</strong>") ;
				return;
			}
			oEditor.SetData($("initOpd").value) ;
			return;
		}
		importFckJs();
    	var pmEditor = new FCKeditor('operDataRichText') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('operDataRichText') ;
		if($("testCaseId").value == ""){
			oEditor.SetData("<strong>" +$("mdPath").value+"</strong>") ;
			return;
		}
		oEditor.SetData($("initOpd").value) ;
	}


	var DropList ="";
	function findInit(){
		if(DropList!=""){
			fW_ch.show();
			fW_ch.bringToTop();
			return;
		}
		var url = conextPath+"/caseManager/caseManagerAction!initDropList.action?dto.isAjax=true";
		var ajaxResut = postSub(url,"");
		if(ajaxResut.indexOf("success")==0){
			DropList = ajaxResut;
			var restArr = ajaxResut.split("$");
			loadSel("caseTypeIdF",restArr[1],"^");
			loadSel("priIdF",restArr[2],"^");
		}
		fW_ch = initW_ch(fW_ch, "findDiv", true, 550, 150);
		fW_ch.setText("查询---当前测试需求:" +treeWin.tree.getItemText($("currNodeId").value));	
	}
	function findExe(){
		$("currNodeIdF").value = $("currNodeId").value;
		var url = pageBreakUrl + "?dto.isAjax=true";
		var ajaxRest = postSub(url, "findForm");
		if(ajaxRest=="failed"){
			hintMsg("查询失败");	
		}else{
			if(ajaxRest.split("$")[1]!=""){
				$("listStr").value=ajaxRest;
				colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				fW_ch.hide();
				fW_ch.setModal(false);
				cp2Pform();
				return;		
			}
			hintMsg("没有查询到相关记录");
		}	
	}
	function swTestTask(){	
		var url = onextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action";
		parent.location = url;
	}