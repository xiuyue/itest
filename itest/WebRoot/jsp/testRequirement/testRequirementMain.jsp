<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<HTML>
<HEAD>
	<TITLE>MYPM项目管理平台</TITLE>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/layout/codebase/skins/dhtmlxlayout_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/layout/codebase/dhtmlxlayout.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
</HEAD>
<BODY style="overflow-y:hidden;">
   <ww:hidden id="repTree" name="analysisDto.treeStr"></ww:hidden>
	<table cellspacing="0" cellpadding="0" align="center" border="0">
		<tr><td><div id="analysisMainDiv" style="width:600px; height:590px"></div></td></tr>
	</table>
</BODY>
<script type="text/javascript">
	var projectSelectW_ch, parameterW_ch;
	$("analysisMainDiv").style.width = clientWidth - 12;
	$("analysisMainDiv").style.height = document.body.clientHeight - 20;
	var mypmLayout = new dhtmlXLayoutObject("analysisMainDiv", "2U");
	var views = mypmLayout.listViews();
	mypmLayout.items[0].setText("测试需求");
	mypmLayout.items[0].setWidth(220);
	mypmLayout.items[1].hideHeader();
	mypmLayout.items[1].attachURL(conextPath+"/outLineManager/outLineAction!index.action");
	//mypmLayout.items[0].attachURL(conextPath+"/outLineManager/outLineAction!loadTree.action");
	//var mypmTree = mypmLayout.items[0].attachTree(0);
	//mypmTree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
	//mypmTree.enableHighlighting(1);
	//mypmTree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
	//mypmTree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
	//mypmTree.setStdImages("book.gif","books_open.gif","books_close.gif");
	//mypmTree.setOnClickHandler(onclickHdl);

</script>
</html>